// node optimizers\r.js -o baseUrl=. name=optimizers/almond include=forms/meo/yearly_region/extension out=..\built\myr.js wrap=true
({
    baseUrl: "..",
    include: ['forms/meo/yr/e_yearly_region'],
    name: "optimizers/almond",
    out: "..\\built\\myr.js",
    wrap: true,
    map:
    {
      '*':
      {
        tpl: 'js/libs/tpl',
        txt: 'js/libs/text'
      }
    }
})