﻿define([
	  'forms/base/nailing/fastening/fc_abstract'
	, 'forms/base/nailing/fastening/h_fastening_clip'
]
, function (w_abstract, h_fastening_clip)
{
	var fastening_with_model_selector = w_abstract();

	fastening_with_model_selector.match = function (adom_item, tag_name, fc_type)
	{
		return 'with' == fc_type;
	}

	fastening_with_model_selector.load_from_model = function (model, model_selector, dom_item, fc_data)
	{
		var self = fastening_with_model_selector;
		h_fastening_clip.each_first_level_fastening(dom_item, function (child_dom_item)
		{
			var child_fc_methods = self.h_fastening_clips.find_methods_for(child_dom_item);
			var child_fc_id = child_dom_item.attr('fc-id');
			var child_model_selector = child_dom_item.attr('model-selector');
			var child_fc_data = !fc_data ? null : fc_data.children[child_fc_id];
			child_fc_data = child_fc_methods.load_from_model(model, child_model_selector, child_dom_item, child_fc_data);
			if (child_fc_data)
				child_fc_data.id = child_fc_id;
			if (!fc_data)
				fc_data = { children: {} };
			fc_data.children[child_fc_id] = child_fc_data;
		});
		return fc_data;
	}

	fastening_with_model_selector.save_to_model = function (model, model_selector, adom_item, fc_data)
	{
		var self = fastening_with_model_selector;
		var dom_item = $(adom_item);
		var fc_model = h_fastening_clip.get_model_field_value(model, model_selector);
		h_fastening_clip.each_first_level_fastening(dom_item, function (child_dom_item)
		{
			var child_fc_methods = self.h_fastening_clips.find_methods_for(child_dom_item);
			var child_fc_id = child_dom_item.attr('fc-id');
			var child_model_selector = child_dom_item.attr('model-selector');
			var child_fc_data = !fc_data ? null : fc_data.children[child_fc_id];
			fc_model = child_fc_methods.save_to_model(fc_model, child_model_selector, child_dom_item, child_fc_data);
		});
		model = h_fastening_clip.set_model_field_value(model, model_selector, fc_model);
		return model;
	}

	fastening_with_model_selector.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.start_with = function ()
		{
			this.push_fc_context();
		}

		_template_argument.with_attrs = function (model_selector)
		{
			return this.fastening_attrs(model_selector, 'with');
		}

		_template_argument.end_with = function ()
		{
			var fc_children_data = this.pop_fc_context();
			this.store_fc_data(null, fc_children_data);
		}
	}

	return fastening_with_model_selector;
});