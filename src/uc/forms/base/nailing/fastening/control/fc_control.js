define([
	  'forms/base/nailing/fastening/fc_abstract'
	, 'forms/base/nailing/fastening/h_fastening_clip'
]
, function (w_abstract, h_fastening_clip)
{
	var fc_control = w_abstract();

	fc_control.match = function (adom_item, tag_name, fc_type)
	{
		return 'DIV' == tag_name && 'control' == fc_type;
	}

	fc_control.render = function (options, model, model_selector, adom_item, fc_data)
	{
		if (!options)
		{
			var dom_item = $(adom_item);
			dom_item.html(dom_item.html() + '<br/>absent options for "' + model_selector + '"!');
		}
		else if (!options.controller)
		{
			var dom_item = $(adom_item);
			dom_item.html(dom_item.html() + '<br/>absent controller for "' + model_selector + '"!');
		}
		else
		{
			var controller = options.controller();

			var value = !model ? null : h_fastening_clip.get_model_field_value(model, model_selector);
			if (!value)
			{
				controller.CreateNew($(adom_item).selector);
			}
			else
			{
				controller.SetFormContent(value);
				controller.Edit($(adom_item).selector);
			}

			if (!fc_data)
			{
				fc_data = { controller: controller }
			}
			else
			{
				fc_data.controller = controller;
			}

			return fc_data;
		}
	}

	fc_control.load_from_model = function (model, model_selector, adom_item, fc_data)
	{
		var controller = fc_data.controller;

		var value = !model ? null : h_fastening_clip.get_model_field_value(model, model_selector);

		if (!value)
		{
			controller.CreateNew($(adom_item).selector);
		}
		else
		{
			controller.SetFormContent(value);
			controller.Edit($(adom_item).selector);
		}

		return fc_data;
	}

	fc_control.save_to_model = function (model, model_selector, dom_item, fc_data)
	{
		var value = null;
		if (fc_data && fc_data.controller)
		{
			var controller = fc_data.controller;
			value = null == controller ? null : controller.GetFormContent();
		}
		return h_fastening_clip.set_model_field_value(model, model_selector, value);
	}

	fc_control.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.control = function (model_selector)
		{
			return this.nailed_div(model_selector, 'control');
		}
	}

	return fc_control;
});