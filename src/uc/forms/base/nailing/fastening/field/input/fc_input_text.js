﻿define([
	  'forms/base/nailing/fastening/fc_abstract'
	, 'forms/base/nailing/fastening/h_fastening_clip'
]
, function (fc_abstract, h_fastening_clip)
{
	var fc_text_field = fc_abstract();

	fc_text_field.match = function (dom_item, tag_name, fc_type)
	{
		return 'INPUT' == tag_name;
	}

	fc_text_field.load_from_model = function (model, model_selector, dom_item)
	{
		var value = h_fastening_clip.get_model_field_value(model, model_selector);
		$(dom_item).val(value);
	}

	fc_text_field.save_to_model = function (model, model_selector, dom_item)
	{
		var value = $(dom_item).val();
		model = h_fastening_clip.set_model_field_value(model, model_selector, value);
		return model;
	}

	fc_text_field.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.txt = function (model_selector, rended_as)
		{
			var attrs = this.fastening_attrs(model_selector, rended_as);

			var value = this.value();
			var fixed_value = (!value || null == value) ? '' : value.toString().replace(/\"/g, '&quot;');

			var res = fixed_value + '"' + attrs.substring(0, attrs.length - 2);
			return res;
		}
	}

	return fc_text_field;
});