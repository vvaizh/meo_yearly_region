﻿define([
	  'forms/base/nailing/fastening/fc_abstract'
]
, function (fc_abstract)
{
	var fc_tabs = fc_abstract();

	fc_tabs.match = function (adom_item, tag_name, fc_type)
	{
		return 'DIV' == tag_name && 'tabs' == fc_type;
	}

	fc_tabs.render = function (options, model, model_selector, adom_item)
	{
		$(adom_item).tabs();
	}

	fc_tabs.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.tabs_attrs = function ()
		{
			return this.fastening_attrs(null, 'tabs');
		}
	}

	return fc_tabs;
});