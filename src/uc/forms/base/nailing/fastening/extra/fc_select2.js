﻿define([
	  'forms/base/nailing/fastening/fc_abstract'
	, 'forms/base/nailing/fastening/h_fastening_clip'
]
, function (fc_abstract, h_fastening_clip)
{
	var fc_select2 = fc_abstract();

	fc_select2.match = function (dom_item, tag_name, fc_type)
	{
		return 'select2' == fc_type;
	}

	var fix_options= function(options)
	{
		if (options && options.ajax)
		{
			if (!options.ajax.results)
				options.ajax.results = function (data, page) { return data; }
			if (!options.ajax.data)
				options.ajax.data = function (term, page) { return { q: term, page: page }; }
		}
	}

	fc_select2.render = function (options, model, model_selector, adom_item)
	{
		var dom_item = $(adom_item);
		var tag_name = dom_item.prop("tagName").toUpperCase();
		var ok_render= (options && null != options) || 'SELECT' === tag_name;
		if (ok_render)
		{
			fix_options(options);
			dom_item.select2(options);
			dom_item.attr('fc-select2', 'true');
			if (model)
			{
				var value = h_fastening_clip.get_model_field_value(model, model_selector);
				dom_item.select2('data', value);
			}
		}
	}

	fc_select2.load_from_model = function (model, model_selector, adom_item)
	{
		var dom_item = $(adom_item);
		if ('true' == dom_item.attr('fc-select2'))
		{
			var value = h_fastening_clip.get_model_field_value(model, model_selector);
			dom_item.select2('data', value);
		}
	}

	fc_select2.save_to_model = function (model, model_selector, adom_item)
	{
		var dom_item = $(adom_item);
		if ('true' != dom_item.attr('fc-select2'))
		{
			return null;
		}
		else
		{
			var value = dom_item.select2('data');
			return h_fastening_clip.set_model_field_value(model, model_selector, value);
		}
	}

	fc_select2.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.select2_attrs = function (model_selector)
		{
			return this.fastening_attrs(model_selector, 'select2');
		}
	}

	return fc_select2;
});