﻿define([
	  'forms/base/nailing/fastening/fc_abstract'
	, 'forms/base/nailing/fastening/h_fastening_clip'
]
, function (fc_abstract, h_fastening_clip)
{
	var fc_empty_array= fc_abstract();

	fc_empty_array.match = function (adom_item, tag_name, fc_type)
	{
		return 'array-empty' == fc_type;
	}

	fc_empty_array.save_to_model = function (model, model_selector, dom_item, fc_data)
	{
		if (fc_data && fc_data.data.array)
			model = fc_data.data.array;
		return model;
	}

	fc_empty_array.load_from_model = function (array, model_selector, dom_item, fc_data)
	{
		if (array && null != array && 0 != array.length)
		{
			dom_item.hide();
		}
		else
		{
			dom_item.show();
		}
	}

	fc_empty_array.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.empty_attrs = function ()
		{
			var res = this.fastening_attrs('', 'array-empty');

			var array = this.current_context.model.value;

			if (array && null != array && 0 != array.length)
			{
				res += ' style="display:none" ';
			}
			else
			{
				var fastening = this.current_context.fastening;
				this.store_fc_data({ array: !array ? [] : array });
			}

			return res;
		}
	}

	return fc_empty_array;
});