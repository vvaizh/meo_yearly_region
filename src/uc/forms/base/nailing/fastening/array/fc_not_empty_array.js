﻿define([
	  'forms/base/nailing/fastening/fc_abstract'
	, 'forms/base/nailing/fastening/h_fastening_clip'
]
, function (fc_abstract, h_fastening_clip)
{
	var fc_not_empty_array = fc_abstract();

	fc_not_empty_array.match = function (adom_item, tag_name, fc_type)
	{
		return 'array-not-empty' == fc_type;
	}

	fc_not_empty_array.render = function (options, model, model_selector, dom_item, fc_data)
	{
		return fc_data;
	}

	fc_not_empty_array.save_to_model = function (model, model_selector, dom_item, fc_data)
	{
		if (fc_data)
		{
			var len = 0;
			for (var id in fc_data.children)
				len++;
			model = new Array(len);
			for (var id in fc_data.children)
			{
				var fc_item_data = fc_data.children[id];
				model[fc_item_data.data.index] = fc_item_data.data.value;
			}
		}
		var h_fastening_clips = this.h_fastening_clips;
		h_fastening_clip.each_first_level_fastening(dom_item, function (child_dom_item)
		{
			var child_fc_model_selector = child_dom_item.attr('model-selector');
			if ('seed' != child_fc_model_selector)
			{
				var child_fc_id = child_dom_item.attr('fc-id');
				var index = parseInt(child_dom_item.attr('array-index'));
				var child_fc_type = child_dom_item.attr('fc-type');
				var child_fc_methods = h_fastening_clips.find_methods_for(child_dom_item);
				model = child_fc_methods.save_to_model(model, index, child_dom_item, null);
			}
		});
		return model;
	}

	var find_seed= function(dom_item)
	{
		var res = null;
		dom_item.children().each(function (index)
		{
			var child_item = $(this);
			var fc_type = child_item.attr('fc-type');
			if ('array-item' == fc_type)
			{
				res = child_item;
				return false;
			}
			var seed = find_seed(child_item);
			if (null != seed)
			{
				res = seed;
				return false;
			}
		});
		return res;
	}

	var collect_dom_items_fc_data = function (seed, fc_data, old_array_info)
	{
		var item_dom_item = seed.next();
		var i = 0;
		var max_fc_id = 0;
		while (item_dom_item && null != item_dom_item && 1 == item_dom_item.length && i < 10)
		{
			var array_index = parseInt(item_dom_item.attr('array-index'));
			var fc_id = parseInt(item_dom_item.attr('fc-id'));
			if (fc_id > max_fc_id)
				max_fc_id = fc_id;
			var item_info = { dom_item: item_dom_item, fc_id: fc_id };
			item_info.fc_data = fc_data.children[fc_id].data;
			for (var i = old_array_info.length; i <= array_index; i++)
			{
				if (i >= old_array_info.length)
					old_array_info.push(null);
			}
			old_array_info[array_index] = item_info;
			item_dom_item = item_dom_item.next();
			i++;
		}
		return max_fc_id;
	}

	var find_old_index = function (old_array_info, new_array_item)
	{
		for (var i= 0; i<old_array_info.length; i++)
		{
			var oai = old_array_info[i];
			if (!oai.index_in_new_array && 0 != oai.index_in_new_array && new_array_item == oai.fc_data.value)
				return i;
		}
		return null;
	}

	var fill_dom_item = function (dom_item, item_model, h_fastening_clips, index)
	{
		dom_item.attr('model-selector', '[' + index + ']');
		dom_item.attr('array-index', index);

		h_fastening_clip.each_first_level_fastening(dom_item, function (child_dom_item)
		{
			var child_fc_id = child_dom_item.attr('fc-id');
			var child_fc_type = child_dom_item.attr('fc-type');
			var child_fc_model_selector = child_dom_item.attr('model-selector');
			var child_fc_methods = h_fastening_clips.find_methods_for(child_dom_item);
			child_fc_methods.load_from_model(item_model, child_fc_model_selector, child_dom_item, null);
		});
	}

	var append_new_array_item = function (seed, last_loaded_item, fc_data, h_fastening_clips)
	{
		var new_dom_item = seed.clone();
		new_dom_item.removeClass('seed');
		new_dom_item.insertAfter(last_loaded_item);
		new_dom_item.attr('fc-id', fc_data.id);
		new_dom_item.show();

		fill_dom_item(new_dom_item, fc_data.data.value, h_fastening_clips, fc_data.data.index);

		return new_dom_item;
	}

	var collect_data_for_new_array = function (array, old_array_info)
	{
		var new_array_info = [];
		for (var i = 0; i < array.length; i++)
		{
			var new_array_item = array[i];
			var index_in_old_array = find_old_index(old_array_info, new_array_item);
			new_array_info[i] = { index_in_old_array: index_in_old_array };
			if (null != index_in_old_array)
				old_array_info[index_in_old_array].index_in_new_array = i;
		}
		return new_array_info;
	}

	var drop_deleted_items = function (old_array_info, fc_data)
	{
		var last_index_in_new_array = 0;
		for (var i = 0; i < old_array_info.length; i++)
		{
			var oai = old_array_info[i];
			if (!oai.index_in_new_array && 0 != oai.index_in_new_array)
			{
				oai.dom_item.detach();
				delete fc_data.children[oai.fc_id];
			}
			else
			{
				var index_in_new_array = oai.index_in_new_array;
				if (index_in_new_array >= last_index_in_new_array)
				{
					last_index_in_new_array = index_in_new_array;
				}
				else
				{
					oai.dom_item.detach();
					oai.detached = true;
				}
			}
		}
	}

	var load_from_non_empty_array = function (self, array, dom_item, fc_data)
	{
		var seed = find_seed(dom_item);

		var old_array_info = [];
		var max_fc_id = collect_dom_items_fc_data(seed, fc_data, old_array_info);

		var new_array_info = collect_data_for_new_array(array, old_array_info);

		if (!fc_data || null == fc_data)
			fc_data = { children: {} };

		drop_deleted_items(old_array_info, fc_data);

		var last_loaded_item= seed;
		for (var i = 0; i < array.length; i++)
		{
			var index_in_old_array = new_array_info[i].index_in_old_array;
			if (null != index_in_old_array && old_array_info[index_in_old_array].fc_data.value == array[i])
			{
				var oai = old_array_info[index_in_old_array];
				fc_data.children[oai.fc_id].data.index = i;
				if (true == oai.detached)
					oai.dom_item.insertAfter(last_loaded_item);
				fill_dom_item(oai.dom_item, oai.fc_data.value, self.h_fastening_clips, i);
				last_loaded_item = oai.dom_item;
			}
			else
			{
				max_fc_id++;
				var fc_id = max_fc_id;
				new_array_info[i].fc_id = fc_id;
				var new_array_item_fc_data = { id: fc_id, data: { index: i, value: array[i] } };
				fc_data.children[fc_id] = new_array_item_fc_data;
				last_loaded_item = append_new_array_item(seed, last_loaded_item, new_array_item_fc_data, self.h_fastening_clips);
			}
		}
		return fc_data;
	}

	fc_not_empty_array.load_from_model = function (array, model_selector, dom_item, fc_data)
	{
		if (array && null != array)
			fc_data = load_from_non_empty_array(this, array, dom_item, fc_data);
		if (!array || null == array || 0 == array.length)
		{
			dom_item.hide();
		}
		else
		{
			dom_item.show();
		}
		return fc_data;
	}

	fc_not_empty_array.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.not_empty_attrs = function ()
		{
			var res = this.fastening_attrs('', 'array-not-empty');

			var array = this.current_context.model.value;

			if (!array || null == array || 0 == array.length)
			{
				res += ' style="display:none" ';
			}
			else
			{
				var fastening = this.current_context.fastening;
			}
			this.push_fc_context();
			this.push_fc_context();
			return res;
		}

		_template_argument.iterate = function ()
		{
			var array_model = this.current_context.parent_context.model;
			if (!array_model.index && 0 != array_model.index)
			{
				array_model.index = -1;
				return true;
			}
			else
			{
				array_model.index++;
				var fc_item_data = this.pop_fc_context();
				
				var array = array_model.value;
				var index = array_model.index - 1;
				if (index >= 0)
					this.store_fc_data({ index: index, value: array[index] }, fc_item_data);

				var array_len = !array || null == array ? 0 : array.length;
				var ok_iterate = array_model.index < array_len;
				if (!ok_iterate)
				{
					var fc_array_data = this.pop_fc_context();
					this.store_fc_data(null, fc_array_data);
				}
				else
				{
					
					this.push_fc_context();
					var current_model = this.current_context.model;
					current_model.value = array_model.value[array_model.index];
					current_model.selector = null;
					current_model.index = null;
				}
				return ok_iterate;
			}
		}

		_template_argument.is_seed = function ()
		{
			var array_model = this.current_context.model;
			return -1 == array_model.index;
		}
	}

	return fc_not_empty_array;
});