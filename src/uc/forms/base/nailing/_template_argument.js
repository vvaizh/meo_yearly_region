﻿define([
	  'forms/base/nailing/fastening/h_fastening_clips'
	, 'forms/base/nailing/fastening/h_fastening_clip'
], function (h_fastening_clips, h_fastening_clip)
{
	return function(model)
	{
		var _template_argument =
			{
				current_context:
					{
						parent_context: null
						, fastening:
							{
								index: 0
								//, data: {}
							}
						, model:
							{
								value: model
								, index: null
								, selector: null
							}
					}
			};

		_template_argument.h_fastening_clip = h_fastening_clip;

		_template_argument.fastening_attrs = function (model_selector, render_as, fastening)
		{
			var res = ' nailed-by="cpw" ';
			if ('' == model_selector || model_selector && null != model_selector)
			{
				res += 'model-selector="' + model_selector + '" ';
				this.current_context.model.selector = model_selector;
			}
			if (render_as && null != render_as)
				res += 'fc-type="' + render_as + '" ';
			if (!fastening)
				fastening = this.current_context.fastening;
			res += 'fc-id="' + fastening.index + '" ';
			fastening.index++;
			return res;
		},

		_template_argument.value= function()
		{
			var res = (null == this.current_context.model.selector) 
				? this.current_context.model.value
				: h_fastening_clip.get_model_field_value(this.current_context.model.value, this.current_context.model.selector);
			return res;
		}

		_template_argument.push_fc_context= function()
		{
			this.current_context =
				{
					parent_context: this.current_context
					, fastening:
						{
							index: 0
							//, data: {}
						}
					, model:
						{
							value: this.value()
							, index: null
							, selector: null
						}
				};
		}

		_template_argument.store_fc_data= function(fc_data, fc_children)
		{
			
			if (!this.current_context.fastening || null == this.current_context.fastening)
				this.current_context.fastening = {};
			var bp = this.current_context.fastening;
			if (!fc_data || null == fc_data)
			{
				if (fc_children && null != fc_children)
				{
					var id = bp.index - 1;
					if (!bp.data)
						bp.data = {};
					bp.data[id] = { id: id, children: fc_children };
				}
			}
			else
			{
				var id = bp.index - 1;
				if (!bp.data)
					bp.data= {};
				if (!fc_children || null == fc_children)
				{
					bp.data[id] = { id: id, data: fc_data };
				}
				else
				{
					bp.data[id] = { id: id, data: fc_data, children: fc_children };
				}
			}
		}

		_template_argument.pop_fc_context= function()
		{
			var fc_data = this.current_context.fastening.data;
			this.current_context = this.current_context.parent_context;
			var fastening = this.current_context.fastening;
			return fc_data;
		}

		_template_argument.nailed_div= function(model_selector,render_as)
		{
			var res = '<div' + this.fastening_attrs(model_selector, render_as) + '>';

			res += 'There should be \"' + render_as + '\" for \"' + model_selector + '\"!';

			return res + '</div>';
		}

		h_fastening_clips.add_template_argument_methods(_template_argument);

		return _template_argument;
	}
}
);