﻿define([
	  'forms/base/nailing/test/nft_default'
	, 'forms/base/nailing/test/nft_select2'
	, 'forms/base/nailing/test/nft_typeahead'
	, 'forms/base/nailing/test/nft_checkbox'
	, 'forms/base/nailing/test/nft_radio'
]
, function (nft_default)
{
	var nailed_field_testers= arguments;
	return {

		find_tester_for: function(adom_item)
		{
			var dom_item= $(adom_item);
			var tag_name = dom_item.prop("tagName").toUpperCase();
			var fc_type = dom_item.attr('fc-type');
			for (var i = 1; i < nailed_field_testers.length; i++)
			{
				var nailed_field_tester = nailed_field_testers[i];
				
				if (nailed_field_tester.match(dom_item, tag_name, fc_type))
					return nailed_field_tester;
			}
			return nft_default;
		}

	};
});