﻿define([
	  'forms/base/nailing/test/nft_abstract'
]
, function (nft_abstract)
{
	var nailed_select2_tester = nft_abstract();

	nailed_select2_tester.match = function (dom_item, tag_name, fc_type)
	{
		return 'select2' == fc_type;
	}

	var GetSelect2_sel_for_dom_item= function(dom_item)
	{
		return '#' + dom_item.prev().attr('id');
	}

	var FocusSelect2_sel = function (sel, dom_item)
	{
		$(sel + ' .select2-input').focus();
		$(sel + ' .select2-choice').click().mousedown().mouseup();
		$(sel + ' .select2-input').focus();
	}

	var FocusSelect2_dom_item= function(dom_item)
	{
		var sel = GetSelect2_sel_for_dom_item(dom_item);
		FocusSelect2_sel(sel, dom_item);
	}

	nailed_select2_tester.check_value = function (dom_item, avalue, instead_of_set)
	{
		var sel = GetSelect2_sel_for_dom_item(dom_item);
		$(sel).addClass('select2-container-active');

		var aevalue = dom_item.select2('data');

		if ('string' == typeof avalue)
		{
			if (-1!=aevalue.text.indexOf(avalue))
			{
				return null; // 'ok, value is "' + value + '"';
			}
			else
			{
				return ' single value "' + aevalue.text + '"! is WRONG!!!!!! it does not contain "' + avalue + '"';
			}
		}
		else
		{
			if (aevalue.length != avalue.length)
			{
				return ' value.length "' + JSON.stringify(aevalue) + '"! is WRONG!!!!!! it should contain ' + avalue.length + ' items!';
			}
			else
			{
				for (var i = 0; i < aevalue.length; i++)
				{
					if (-1==aevalue[i].text.indexOf(avalue[i]))
					{
						return ' value "' + JSON.stringify(aevalue) + '"! is WRONG!!!!!! it does not contain "' + avalue[i] + '"';
					}
				}
				return null; // 'ok, value is "' + value + '"';
			}
		}
	}

	var set_value= function (dom_item, value, on_finish)
	{
		var sel = GetSelect2_sel_for_dom_item(dom_item);

		var bind = function (p) { dom_item.bind(p.event, p.handler); }
		var unbind = function (p) { dom_item.unbind(p.event, p.handler); }

		var set_value_result = null;

		var return_when_closed = { num: 2, event: 'select2-close' };
		return_when_closed.handler = function ()
		{
			var self = return_when_closed;
			unbind(self);
			on_finish(set_value_result);
		};

		var select_first_item_in_dropdown = { num: 2, event: 'select2-loaded' };
		select_first_item_in_dropdown.handler = function ()
		{
			var self = select_first_item_in_dropdown;
			var labels = $('#select2-drop .select2-result-label');
			unbind(self);
			bind(return_when_closed);
			if (1 > labels.length || -1 == $(labels[0]).text().indexOf(value))
			{
				set_value_result = false;
			}
			else
			{
				set_value_result = true;
				$('#select2-drop ul li').first().mousedown().mouseup().click();
			}
			setTimeout(function () { dom_item.select2('close'); }, 300);
		};

		var set_input_value = function (dom_input)
		{
			dom_input.val('').change().keydown().keyup().change();
			bind(select_first_item_in_dropdown);
			dom_input.val(value).change().keydown().keyup().change();
		}

		var set_input_value_when_drop_down_opened = { num: 1, event: 'select2-loaded' };
		set_input_value_when_drop_down_opened.handler = function ()
		{
			unbind(set_input_value_when_drop_down_opened);
			set_input_value($('#select2-drop .select2-input'));
		}

		if (!$(sel).hasClass('select2-container-multi'))
		{
			bind(set_input_value_when_drop_down_opened);
			FocusSelect2_sel(sel, dom_item);
		}
		else
		{
			FocusSelect2_sel(sel, dom_item);
			set_input_value($(sel + ' .select2-input'));
		}
	}

	var set_single_value = function (dom_item, value, model_selector)
	{
		set_value(dom_item, value, function (set_value_result)
		{
			app.wbt_long_process_result =
				(true === set_value_result) ? '\'' + model_selector + '\': ' + 'selected text "' + value + '"' :
				(false === set_value_result) ? '\'' + model_selector + '\': ' + 'can not find text "' + value + '" to select !!!!!!!!!' :
											'\'' + model_selector + '\': ' + 'can not set "' + value + '" to select !!!!!!!!!';
		});
		return "wait_long_process!";
	}

	var set_array_value = function (dom_item, value, model_selector)
	{
		dom_item.select2('data', []);
		var i = 0;
		if (i >= value.length)
		{
			return 'set value []';
		}
		else
		{
			var on_set_value;
			on_set_value = function (set_value_result)
			{
				if (true !== set_value_result)
				{
					app.wbt_long_process_result = '\'' + model_selector + '\': ' + "can not set value " + JSON.stringify(value);
				}
				else
				{
					i++;
					if (i >= value.length)
					{
						app.wbt_long_process_result = '\'' + model_selector + '\': ' + "selected textes " + JSON.stringify(value);
					}
					else
					{
						set_value(dom_item, value[i], on_set_value);
					}
				}
			}
			set_value(dom_item, value[i], on_set_value);
			return "wait_long_process!";
		}
		
	}

	nailed_select2_tester.set_value = function (dom_item, value, model_selector)
	{
		if ('string'==typeof value)
		{
			return set_single_value(dom_item, value, model_selector);
		}
		else
		{
			return set_array_value(dom_item, value, model_selector);
		}
		
	}

	return nailed_select2_tester;
});