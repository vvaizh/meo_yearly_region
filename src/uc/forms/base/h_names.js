// ГОСТ Р 52535.1-2006
define(function ()
{
	var helper =
	{
		transliteration:
		{
			'А': 'A',
			'Б': 'B',
			'В': 'V',
			'Г': 'G',
			'Д': 'D',
			'Е': 'E',
			'Ё': 'E',
			'Ж': 'ZH',
			'З': 'Z',
			'И': 'I',
			'Й': 'I',
			'К': 'K',
			'Л': 'L',
			'М': 'M',
			'Н': 'N',
			'О': 'O',
			'П': 'P',
			'Р': 'R',
			'С': 'S',
			'Т': 'T',
			'У': 'U',
			'Ф': 'F',
			'Х': 'KH',
			'Ц': 'TC',
			'Ч': 'CH',
			'Ш': 'SH',
			'Щ': 'SHCH',
			'Ы': 'Y',
			'Э': 'E',
			'Ю': 'IU',
			'Я': 'IA',
			'Ъ': '',
			'Ь': '',
			' ': ' '
		}

		,transencodingliteration:
		{
			'À': 'А',
			'Á': 'Б',
			'Â': 'В',
			'Ã': 'Г',
			'Ä': 'Д',
			'Å': 'Е',
			'Æ': 'Ж',
			'Ç': 'З',
			'È': 'И',
			'É': 'Й',
			'Ê': 'К',
			'Ë': 'Л',
			'Ì': 'М',
			'Í': 'Н',
			'Î': 'О',
			'Ï': 'П',
			'Ð': 'Р',
			'Ñ': 'С',
			'Ò': 'Т',
			'Ó': 'У',
			'Ô': 'Ф',
			'Õ': 'Х',
			'Ö': 'Ц',
			'×': 'Ч',
			'Ø': 'Ш',
			'Ù': 'Щ',
			'Ú': 'Ъ',
			'Û': 'Ы',
			'Ü': 'Ь',
			'Ý': 'Э',
			'Þ': 'Ю',
			'ß': 'Я',
			'à': 'а',
			'á': 'б',
			'â': 'в',
			'ã': 'г',
			'ä': 'д',
			'å': 'е',
			'æ': 'ж',
			'ç': 'з',
			'è': 'и',
			'é': 'й',
			'ê': 'к',
			'ë': 'л',
			'ì': 'м',
			'í': 'н',
			'î': 'о',
			'ï': 'п',
			'ð': 'р',
			'ñ': 'с',
			'ò': 'т',
			'ó': 'у',
			'ô': 'ф',
			'õ': 'х',
			'ö': 'ц',
			'÷': 'ч',
			'ø': 'ш',
			'ù': 'щ',
			'ú': 'ъ',
			'û': 'ы',
			'ü': 'ь',
			'ý': 'э',
			'þ': 'ю',
			'ÿ': 'я'
		}

		,NormalizeNameParts: function(txt, separator)
		{
			var res = '';
			var maleMiddlePart = "оглы, улы, уулу";
			var femaleMiddlePart = "кызы, гызы";
			var parts = txt.split(separator);
			for (var i = 0; i < parts.length; i++)
			{
				if (0 != i)
					res += separator;
				var part = parts[i];
				res += -1 != maleMiddlePart.indexOf(part) || -1 != femaleMiddlePart.indexOf(part) ? part : part.substr(0, 1).toUpperCase() + part.substr(1);
			}
			return res;
		}

		,NormalizeNameItem: function (txt)
		{
			var res = txt.toLowerCase();
			res = helper.NormalizeNameParts(res, ' ');
			res = helper.NormalizeNameParts(res, '-');
			return res;
		}

		,NormalizeEncodingNameItem: function (txt)
		{
			var res = '';
			for (var i = 0; i < txt.length; i++)
			{
				var c = txt.substr(i, 1);
				if (!helper.transencodingliteration[c])
				{
					res += c;
				}
				else
				{
					res += helper.transencodingliteration[c];
				}
			}
			return res;
		}

		,PrepareLatinNameItem: function (txt)
		{
			var res = '';
			for (var i = 0; i < txt.length; i++)
			{
				var c = txt.substr(i, 1).toUpperCase();
				if (!helper.transliteration[c])
				{
					//res += c;
				}
				else
				{
					res += helper.transliteration[c];
				}
			}
			return helper.NormalizeNameItem(res);
		}

		,OnNameChange_NormalizeAndLatinize: function (e)
		{
			var el = $(e.target);
			var orig_value = el.val();
			var fixed_value = helper.NormalizeEncodingNameItem(orig_value);
			fixed_value = helper.NormalizeNameItem(fixed_value);
			el.val(fixed_value);

			var el_id = el.attr('id');
			var latin_el_id = el_id + '_latin'
			$('#' + latin_el_id).val(helper.PrepareLatinNameItem(fixed_value)).change();
		}

		,OnNameChange_NormalizeEncoding: function (e) {
			var el = $(e.target);
			var orig_value = el.val();
			var fixed_value = helper.NormalizeEncodingNameItem(orig_value);
			el.val(fixed_value);
		}
	};
	return helper;
});
