define([
	'forms/base/codec/codec.copy'
]
, function (codec_copy)
{
	var ccodec_copy = codec_copy();

	var rowset = null;

	rowset = function ()
	{
		return {
			rows: []

			,where: function(f)
			{
				var res = rowset();
				for (var i = 0; i < this.rows.length; i++)
				{
					var row = this.rows[i];
					if (f(row))
						res.rows.push(ccodec_copy.Copy(row));
				}
				return res;
			}

			,inner_join: function(arr,alias,fon)
			{
				var res = rowset();
				for (var i = 0; i < this.rows.length; i++)
				{
					var row = this.rows[i];
					for (var j= 0; j<arr.length; j++)
					{
						row[alias] = arr[j];
						if (fon(row))
							res.rows.push(ccodec_copy.Copy(row));
					}
					delete row[alias];
				}
				return res;
			}

			,map: function(f)
			{
				return ql_helper.map(this.rows,f);
			}
		}
	}

	var ql_helper = {
		from: function (arr, alias)
		{
			var res = rowset();
			for (var i = 0; i < arr.length; i++)
			{
				var row = {};
				var fields = {}
				row[alias] = fields;
				ccodec_copy.CopyFieldsTo(arr[i], fields);
				res.rows.push(row);
			}
			return res;
		}
		, map: function (arr,f)
		{
			var res = [];
			for (var i = 0; i < arr.length; i++)
			{
				res.push(f(arr[i]));
			}
			return res;
		}
		, find_first_or_null: function (arr, f)
		{
			for (var i = 0; i < arr.length; i++)
			{
				var row = arr[i];
				if (f(row))
					return row;
			}
			return null;
		}
		, next_id: function (arr, id_name)
		{
			var res = 0;
			for (var i = 0; i < arr.length; i++)
			{
				var row = arr[i];
				var id = row[id_name];
				if (res <= id)
					res = id + 1;
			}
			return res;
		}
		, insert_with_next_id: function (arr, id_name, new_row)
		{
			var id = this.next_id(arr, id_name);
			new_row[id_name] = id;
			arr.push(new_row);
			return new_row;
		}
		, delete: function (arr, f)
		{
			var res = [];
			for (var i = 0; i < arr.length; i++)
			{
				var row = arr[i];
				if (!f(row))
					res.push(row);
			}
			return res;
		}
	}

	return ql_helper;
});
