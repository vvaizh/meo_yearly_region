define(function ()
{
	var helper_res =
	{
		Create_OnOpen_ForMsgBoxWithController: function (options)
		{
			return function ()
			{
				var id_div = options.id_div;
				if (!id_div)
					id_div = 'cpw-msgbox';
				var box_sel = '#' + id_div;

				options.controller.Edit(box_sel);
			}
		}

		, Create_OnClose_ForMsgBoxWithController: function (options)
		{
			return function ()
			{
				var id_div = options.id_div;
				if (!id_div)
					id_div = 'cpw-msgbox';
				var box_sel = '#' + id_div;

				if (options.controller)
				{
					options.controller.Destroy();
					options.controller = null;
				}
				var box = $(box_sel);
				box.dialog('destroy');
				box.html('');
			}
		}

		, PrepareDiv: function (options)
		{
			var id_div = options.id_div;
			if (!id_div)
				id_div = 'cpw-msgbox';
			var box_sel = '#' + id_div;

			var box = $(box_sel);
			if (0 == box.length)
			{
				$('body').prepend('<div id="' + id_div + '" class="cpw-msgbox" style="display:none">Здесь будет всплывающая форма</div>');
				box = $(box_sel);
			}
			return box;
		}

		, Close: function (options)
		{
			var id_div = 'cpw-msgbox';
			if (options && options.id_div)
				id_div = options.id_div;
			var box_sel = '#' + id_div;
			$(box_sel).dialog("close");
		}

		, ShowModal: function (options)
		{
			if (!options)
				options= {};
			if (!options.modal)
				options.modal = true;
			this.Show(options);
		}

		, Show: function (options)
		{
			var box = this.PrepareDiv(options);

			if (options.html)
			{
				box.html(options.html);
				options.close = this.Create_OnClose_ForMsgBoxWithController(options);
				delete options.html;
			}
			else if (options.controller)
			{
				box.html('');
				options.open = this.Create_OnOpen_ForMsgBoxWithController(options);
				options.close = this.Create_OnClose_ForMsgBoxWithController(options);
				if (options.controller.size)
				{
					options.width = options.controller.size.width;
					options.height = options.controller.size.height;
				}
			}
			else if (options.title)
			{
				box.html(options.title);
			}

			if (!options.modal)
				options.modal = true;
			if (!options.modal)
				options.resizable = false;

			var buttons = (!options.buttons) ? ['OK'] : options.buttons;

			var on_close = options.onclose ? options.onclose : function (bname) { };
			var on_after_close = options.onafterclose ? options.onafterclose : function (bname) { };
			delete options.onclose;

			options.buttons = {};
			for (var i = 0; i < buttons.length; i++)
			{
				var button_name = buttons[i];
				options.buttons[button_name] =
					(function (bname, onclose, onafterclose)
					{
						return function ()
						{
							var res = onclose(bname,this);
							if (false != res)
							{
								$(this).dialog("close");
								onafterclose(bname);
							}
						}
					})
				(button_name, on_close, on_after_close);
			}

			options.position = { my: 'center', at: 'center', of: window };

			box.dialog(options);
		}

		, ShowAjaxError: function (title, url, data, textStatus)
		{
			var error_html = title + '<br/>';
			error_html += 'url: <b><a href="' + url + '">' + url + '</a></b>!<br/>\r\n';
			error_html += 'textStatus: <b>' + textStatus + '</b><br/>\r\n';
			error_html += 'data: <b>' + JSON.stringify(data) + '</b><br/>\r\n';
			this.ShowModal({ html: error_html, width: 600, height: 400, title: 'Ошибка ajax запроса' });
		}

		, ShowDeffered: function (options)
		{
			var self = this;
			setTimeout(function () { self.Show(options); }, 50);
		}
	};
	return helper_res;
});
