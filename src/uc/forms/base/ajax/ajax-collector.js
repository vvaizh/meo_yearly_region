define(function ()
{
	return function (transports)
	{
		return function (options, originalOptions, jqXHR)
		{
			for (var i = 0; i < transports.length; i++)
			{
				var t = transports[i];
				if ('function' != typeof t)
					t = transports[i]= t.prepare_try_to_prepare_send_abort();
				var result = t(options, originalOptions, jqXHR);
				if (result)
					return result;
			}
		}
	}
});
