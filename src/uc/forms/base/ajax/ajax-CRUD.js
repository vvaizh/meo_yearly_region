define([
	'forms/base/codec/codec.copy'
]
, function (codec_copy)
{
	return function()
	{
		var transport = {}

		transport.rows_all = [];
		transport.options = {
			id_name: 'id'
			,ParseUrl: function(url)
			{
				if (-1!=url.indexOf('?action=add'))
				{
					return { action: 'c' };
				}
				else
				{
					var prefix = '&' + this.id_name + '=';
					var pos = url.indexOf(prefix);
					var pos_end = url.indexOf('&',pos+1);
					var id = -1 == pos_end 
						? url.substring(pos + prefix.length) 
						: url.substring(pos + prefix.length, pos_end);
					if (-1 != url.indexOf('?action=get&'))
					{
						return { action: 'r', id: id };
					}
					else if (-1 != url.indexOf('?action=update&'))
					{
						return { action: 'u', id: id };
					}
					else if (-1 != url.indexOf('?action=delete&'))
					{
						return { action: 'd', id: id };
					}
				}
				return false;
			}
		};

		var urldecode = function (str)
		{
			return decodeURIComponent((str + '').replace(/\+/g, '%20'));
		}

		transport.prepare_send_abort = function (options, originalOptions, jqXHR, parsed_args)
		{
			var self = this;
			var send_abort=
			{
				send: function (headers, completeCallback)
				{
					var rows_all = self.rows_all;
					if ('function' == typeof rows_all)
						rows_all = rows_all();
					switch (parsed_args.action)
					{
						case 'c':
							self.create(JSON.parse(urldecode(options.data)));
							completeCallback(200, 'success', { text: '{ "ok": true }' });
							return;
						case 'r':
							var row = self.read(parsed_args.id);
							completeCallback(200, 'success', { text: JSON.stringify(row) });
							return;
						case 'u':
							self.update(parsed_args.id,JSON.parse(urldecode(options.data)));
							completeCallback(200, 'success', { text: '{ "ok": true }' });
							return;
						case 'd':
							self.delete(parsed_args.id);
							completeCallback(200, 'success', { text: '{ "ok": true }' });
							return;
					}

					completeCallback(200, 'success', { ok: true });
				}
				, abort: function ()
				{
				}
			}
			return send_abort;
		};

		transport.prepare_try_to_prepare_send_abort = function ()
		{
			var self = this;
			return function (options, originalOptions, jqXHR)
			{
				if (0 == options.url.indexOf(self.options.url_prefix))
				{
					var parsed_args = self.options.ParseUrl(options.url);
					if (false != parsed_args)
					{
						return self.prepare_send_abort(options, originalOptions, jqXHR, parsed_args);
					}
				}
			}
		}

		return transport;
	}
});
