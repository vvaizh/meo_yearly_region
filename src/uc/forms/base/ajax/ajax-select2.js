define([
	'forms/base/codec/codec.copy'
]
, function (codec_copy)
{
	return function()
	{
		var transport = {}

		transport.query = function ()
		{
			return [{ id: 1, text: "1" }, { id: 2, text: "2" }];
		}

		transport.options = {
		}

		var urldecode = function (str)
		{
			return decodeURIComponent((str + '').replace(/\+/g, '%20'));
		}

		var ParseUrl= function(url)
		{
			url = urldecode(url);
			var parsed_args = { q: '' };

			var prefix= 'q=';
			var pos = url.indexOf(prefix);
			if (-1 != pos)
			{
				var pos_end = url.indexOf('&', pos + 1);
				parsed_args.q = -1 == pos_end
						? url.substring(pos + prefix.length)
						: url.substring(pos + prefix.length, pos_end);
			}

			var prefix = 'page=';
			var pos = url.indexOf(prefix);
			if (-1 != pos)
			{
				var pos_end = url.indexOf('&', pos + 1);
				parsed_args.page = -1 == pos_end
						? url.substring(pos + prefix.length)
						: url.substring(pos + prefix.length, pos_end);
			}

			return parsed_args;
		}

		transport.prepare_send_abort = function (options, originalOptions, jqXHR)
		{
			var self = this;
			var send_abort=
			{
				send: function (headers, completeCallback)
				{
					var parsed_args = ParseUrl(options.url);
					var res = self.query(parsed_args.q, parsed_args.page);
					completeCallback(200, 'success', { text: JSON.stringify(res) });
				}
				, abort: function ()
				{
				}
			}
			return send_abort;
		};

		transport.prepare_try_to_prepare_send_abort = function ()
		{
			var self = this;
			return function (options, originalOptions, jqXHR)
			{
				if (0 == options.url.indexOf(self.options.url_prefix))
				{
					return self.prepare_send_abort(options, originalOptions, jqXHR);
				}
			}
		}

		return transport;
	}
});
