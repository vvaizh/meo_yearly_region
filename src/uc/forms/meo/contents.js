define
(
	[
		  'forms/base/collector'
		, 'txt!forms/meo/yr/diagnostics/other-method/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/meo/yr/diagnostics/biochemistry/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/meo/yr/diagnostics/biochemistry/tests/contents/03added.json.etalon.txt'
		, 'txt!forms/meo/yr/diagnostics/methods/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/meo/yr/diagnostics/methods/tests/contents/03added.json.etalon.txt'
		, 'txt!forms/meo/yr/organizational/office/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/meo/yr/organizational/offices/tests/contents/01added.json.etalon.txt'
		, 'txt!forms/meo/yr/organizational/unit/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/meo/yr/organizational/units/tests/contents/01added.json.etalon.txt'
		, 'txt!forms/meo/yr/organizational/center/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/meo/yr/organizational/audience/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/meo/yr/organizational/licenses/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/meo/yr/organizational/main/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/meo/yr/economic/static/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/meo/yr/economic/procurement/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/meo/yr/economic/repair/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/meo/yr/economic/service/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/meo/yr/economic/service/tests/contents/03added.json.etalon.txt'
		, 'txt!forms/meo/yr/economic/main/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/meo/yr/main/tests/contents/01sav.etalon.xml'
	],
	function (collect)
	{
		return collect([
		  'yr-diagnostic-other-method-01sav'
		, 'yr-biochemistry-01sav'
		, 'yr-biochemistry-03added'
		, 'yr-diagnostic-methods-01sav'
		, 'yr-diagnostic-methods-03added'
		, 'yr-org-office-01sav'
		, 'yr-org-offices-01added'
		, 'yr-org-unit-01sav'
		, 'yr-org-units-01added'
		, 'yr-org-center-01sav'
		, 'yr-audience-01sav'
		, 'yr-licenses-01sav'
		, 'yr-organizational-01sav'
		, 'yr-static-01sav'
		, 'yr-procurement-01sav'
		, 'yr-repair-01sav'
		, 'yr-service-01sav'
		, 'yr-service-03added'
		, 'yr-economic-01sav'
		, 'yr-01sav'
		], Array.prototype.slice.call(arguments,1));
	}
);