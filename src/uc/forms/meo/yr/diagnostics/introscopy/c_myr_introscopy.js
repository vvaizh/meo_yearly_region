define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/diagnostics/introscopy/e_myr_introscopy.html'
	, 'forms/meo/yr/diagnostics/methods/x_myr_diagnostic_methods'
	, 'forms/meo/yr/diagnostics/methods/c_myr_diagnostic_methods'
	, 'forms/meo/yr/diagnostics/introscopy/h_myr_rentgen_description'
	, 'forms/meo/yr/diagnostics/introscopy/h_myr_ultrasound_description'
],
function (c_nailed, tpl, x_myr_diagnostic_methods, c_myr_diagnostic_methods, h_myr_rentgen_description, h_myr_ultrasound_description)
{
	return function()
	{
		var options = {
			field_spec:
			{
				Рентгеновский_кабинет:
					{ controller: function () { return c_myr_diagnostic_methods(x_myr_diagnostic_methods(h_myr_rentgen_description)); } }
				, Кабинет_ультразвуковой_диагностики:
					{ controller: function () { return c_myr_diagnostic_methods(x_myr_diagnostic_methods(h_myr_ultrasound_description)); } }
			}
		};
		var controller = c_nailed(tpl, options);
		return controller;
	}
});
