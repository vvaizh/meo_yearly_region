define([
	  'forms/meo/yr/diagnostics/methods/c_myr_diagnostic_methods'
	, 'forms/meo/yr/diagnostics/methods/x_myr_diagnostic_methods'
],
function (c_myr_diagnostic_methods, codec_model)
{
	var test_description = {
		Название_раздела: 'Тестовый раздел диагностики'
			, Явно_перечисленные_названия_методов_с_разметкой:
			[
				"метод <strong>1</strong>"
				, "<strong>метод</strong> 2"
				, "метод 3"
				, "<strong>метод 4</strong>"
			]
	};
	var test_codec_model = codec_model(test_description);
	return function ()
	{
		controller = c_myr_diagnostic_methods(test_codec_model);
		return controller;
	}
});
