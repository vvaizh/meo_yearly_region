define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/diagnostics/methods/e_myr_diagnostic_methods.html'
	, 'forms/meo/yr/diagnostics/other-method/c_other_method'
	, 'forms/base/h_msgbox'
	, 'forms/base/nailing/fastening/h_fastening_clip'
	, 'tpl!forms/meo/yr/diagnostics/other-method/v_other_method_confirm_delete.html'
],
function (c_nailed, tpl, c_other_method, h_msgbox, h_fastening_clip, v_other_method_confirm_delete)
{
	return function (codec_model)
	{
		var controller = c_nailed(tpl);

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			if (!this.model)
				this.SetFormContent(codec_model.CreateEmptyModel());
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' button.add').click(function () { self.OnAdd(); });
			this.StaticOnEdit = function (e) { e.preventDefault(); self.OnEdit(e); }
			this.StaticOnDelete = function (e) { e.preventDefault(); self.OnDelete(e); }
			this.StaticOnChangeCheckbox = function (e) { self.OnChangeCheckbox(e.target); };
			this.BindEventHandlers();
		}

		controller.BindEventHandlers= function()
		{
			var sel = this.fastening.selector;
			var self = this;
			$(sel + ' td.extra-name span').off('click', this.StaticOnEdit).on('click', this.StaticOnEdit);
			$(sel + ' [model-selector="Прочие"] td.edit img').off('click', this.StaticOnEdit).on('click', this.StaticOnEdit);
			$(sel + ' [model-selector="Прочие"] td.delete img').off('click', this.StaticOnDelete).on('click', this.StaticOnDelete);
			$(sel + ' input[type="checkbox"]').off('change', this.StaticOnChangeCheckbox).on('change',this.StaticOnChangeCheckbox)
			.each(function () { self.OnChangeCheckbox(this); });
		}

		controller.OnChangeCheckbox = function (aitem)
		{
			var item = $(aitem);
			var label = item.parent('label')
			label.removeClass('no');
			label.removeClass('yes');
			label.addClass('checked' == item.attr('checked') ? 'yes' : 'no');
		}

		controller.OnAdd= function()
		{
			var sel = this.fastening.selector;
			var self = this;
			var c_item = c_other_method({ Раздел: codec_model.Бланк.Название_раздела });
			c_item.SetFormContent({});
			var btnOk = 'Сохранить информацию о применяемом диагностическом методе';
			h_msgbox.ShowModal
			({
				title: 'Добавление применяемого диагностического метода'
				, controller: c_item
				, buttons: ['Отмена', btnOk]
				, id_div: "cpw-form-myr-other-method"
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						var fc_dom_item = $(self.fastening.selector + ' div[model-selector="Прочие"]');
						var model = self.fastening.get_fc_model_value(fc_dom_item);
						if (null == model)
							model = [];
						model.push(c_item.GetFormContent());
						self.fastening.set_fc_model_value(fc_dom_item, model);
						self.BindEventHandlers();
					}
				}
			});
		}

		controller.OnEdit = function (e)
		{
			var fastening = this.fastening;
			var i_item = h_fastening_clip.get_parent_fc_of_type($(e.target), 'array-item').attr('array-index');
			var fc_dom_item = $(this.fastening.selector + ' div[model-selector="Прочие"]');
			var model = fastening.get_fc_model_value(fc_dom_item);
			var c_item = c_other_method({ Раздел: codec_model.Бланк.Название_раздела });
			c_item.SetFormContent(model[i_item]);
			var btnOk = 'Сохранить информацию о применяемом диагностическом методе';
			var self = this;
			h_msgbox.ShowModal
			({
				title: 'Редактирование применяемого диагностического метода'
				, controller: c_item
				, buttons: ['Отмена', btnOk]
				, id_div: "cpw-form-myr-other-method"
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						var fc_dom_item = $(self.fastening.selector + ' div[model-selector="Прочие"]');
						var model = self.fastening.get_fc_model_value(fc_dom_item);
						model[i_item] = c_item.GetFormContent();
						self.fastening.set_fc_model_value(fc_dom_item, model);
						self.BindEventHandlers();
					}
				}
			});
		}

		controller.OnDelete = function (e)
		{
			var fastening = this.fastening;
			var i_item = h_fastening_clip.get_parent_fc_of_type($(e.target), 'array-item').attr('array-index');
			var fc_dom_item = $(this.fastening.selector + ' div[model-selector="Прочие"]');
			var model = fastening.get_fc_model_value(fc_dom_item);
			var self = this;

			var btnOk = 'Да, удалить';
			h_msgbox.ShowModal({
				title: 'Подтверждение удаления диагностического метода', width: 590, height: 260
				, html: v_other_method_confirm_delete({ Удаяемый: model[i_item], Раздел: codec_model.Бланк.Название_раздела })
				, buttons: ['Отмена', btnOk]
				, id_div: "cpw-form-myr-other-method-delete-confirm"
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						model.splice(i_item, 1);
						self.fastening.set_fc_model_value(fc_dom_item, model);
						self.BindEventHandlers();
					}
				}
			});
		}

		controller.UseCodec(codec_model);

		return controller;
	}
});
