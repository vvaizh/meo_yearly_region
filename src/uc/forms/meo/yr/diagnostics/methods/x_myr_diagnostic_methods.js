﻿define([
	  'forms/base/codec/codec'
],
function (BaseCodec)
{
	return function (бланк)
	{
		var codec_model = BaseCodec();

		codec_model.Бланк = бланк;

		codec_model.CreateEmptyModel = function ()
		{
			var arr = [];
			var model = { Явно_перечисленные_в_бланке: arr };
			for (var i = 0; i < бланк.Явно_перечисленные_названия_методов_с_разметкой.length; i++)
			{
				var m = бланк.Явно_перечисленные_названия_методов_с_разметкой[i];
				arr.push({ Метод: m });
			}
			return model;
		}

		var убрать_разметку = function (txt)
		{
			return !txt || null==txt ? txt : txt.replace(/\<strong\>/g, '').replace(/\<\/strong\>/g, '');
		}

		codec_model.Encode= function(model)
		{
			if (model.Явно_перечисленные_в_бланке)
			{
				for (var i = 0; i < model.Явно_перечисленные_в_бланке.length; i++)
				{
					var item = model.Явно_перечисленные_в_бланке[i];
					item.Метод = убрать_разметку(item.Метод);
				}
			}
			return model;
		}

		codec_model.Decode = function (model)
		{
			if ('string' == typeof model)
				model = JSON.parse(model);
			if (model.Явно_перечисленные_в_бланке)
			{
				for (var i = 0; i < model.Явно_перечисленные_в_бланке.length; i++)
				{
					var item = model.Явно_перечисленные_в_бланке[i];
					for (var j = 0; j < бланк.Явно_перечисленные_названия_методов_с_разметкой.length; j++)
					{
						var с_разметкой = бланк.Явно_перечисленные_названия_методов_с_разметкой[j];
						var без_разметки = убрать_разметку(с_разметкой);
						if (без_разметки == item.Метод)
						{
							item.Метод = с_разметкой;
							break;
						}
					}
				}
			}
			return model;
		}

		return codec_model;
	}
});
