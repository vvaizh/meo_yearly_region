define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/diagnostics/functional/e_myr_functional.html'
	, 'forms/meo/yr/diagnostics/methods/x_myr_diagnostic_methods'
	, 'forms/meo/yr/diagnostics/functional/h_myr_functional_description'
	, 'forms/meo/yr/diagnostics/methods/c_myr_diagnostic_methods'
],
function (c_nailed, tpl, x_myr_diagnostic_methods, h_myr_functional_description, c_myr_diagnostic_methods)
{
	return function()
	{
		var options = {
			field_spec:
			{
				Отделение_функциональной_диагностики: { controller: function () { return c_myr_diagnostic_methods(x_myr_diagnostic_methods(h_myr_functional_description)); } }
			}
		};

		var controller = c_nailed(tpl, options);
		return controller;
	}
});
