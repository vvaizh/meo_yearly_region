define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/diagnostics/main/e_myr_diagnostic.html'

	, 'forms/meo/yr/diagnostics/general/c_myr_diagnostic_general'
	, 'forms/meo/yr/diagnostics/blood/c_myr_blood'
	, 'forms/meo/yr/diagnostics/biochemistry/c_myr_biochemistry'
	, 'forms/meo/yr/diagnostics/introscopy/c_myr_introscopy'
	, 'forms/meo/yr/diagnostics/biophysics/c_myr_biophysics'
	, 'forms/meo/yr/diagnostics/functional/c_myr_functional'
	, 'forms/meo/yr/diagnostics/other/c_myr_diagnostic_other'
],
function (c_nailed, tpl
	, c_myr_diagnostic_general
	, c_myr_blood
	, c_myr_biochemistry
	, c_myr_introscopy
	, c_myr_biophysics
	, c_myr_functional
	, c_myr_diagnostic_other
	)
{
	return function()
	{
		var options = {
			field_spec:
				{
					Общие: { controller: c_myr_diagnostic_general }
					, Кровь: { controller: c_myr_blood }
					, Биохимия: { controller: c_myr_biochemistry }
					, Интроскопия: { controller: c_myr_introscopy }
					, Биофизика: { controller: c_myr_biophysics }
					, Функциональная: { controller: c_myr_functional }
					, Прочее: { controller: c_myr_diagnostic_other }
				}
		};

		var controller = c_nailed(tpl, options);

		return controller;
	}
});
