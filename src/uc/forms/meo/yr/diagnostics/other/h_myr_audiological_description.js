define(function ()
{
	return {
		Название_раздела: 'Аудиологические исследования'
		,Явно_перечисленные_названия_методов_с_разметкой:
		[
			"тональная пороговая: <strong>аудиометрия</strong>"
			, "<strong>Акуметрия</strong>: проба Вебера, Ринне, Швабаха, Желе"
			, "<strong>Вестибулярные пробы</strong>: вращательная, отолитовая реакция, калорическая проба"
		]
	};
});
