define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/diagnostics/other/e_myr_diagnostic_other.html'
	, 'forms/meo/yr/diagnostics/methods/x_myr_diagnostic_methods'
	, 'forms/meo/yr/diagnostics/methods/c_myr_diagnostic_methods'

	, 'forms/meo/yr/diagnostics/other/h_myr_audiological_description'
	, 'forms/meo/yr/diagnostics/other/h_myr_endoscopy_description'
	, 'forms/meo/yr/diagnostics/other/h_myr_hormones_description'
	, 'forms/meo/yr/diagnostics/other/h_myr_immunology_description'
	, 'forms/meo/yr/diagnostics/other/h_myr_laboratory_description'
],
function (c_nailed, tpl, x_myr_diagnostic_methods, c_myr_diagnostic_methods

	, h_myr_audiological_description
	, h_myr_endoscopy_description
	, h_myr_hormones_description
	, h_myr_immunology_description
	, h_myr_laboratory_description
	)
{
	return function()
	{
		var options = {
			field_spec:
			{
				Иммунологические_исследования:
					{ controller: function () { return c_myr_diagnostic_methods(x_myr_diagnostic_methods(h_myr_immunology_description)); } }
				, Гормональные_исследования:
					{ controller: function () { return c_myr_diagnostic_methods(x_myr_diagnostic_methods(h_myr_hormones_description)); } }
				, Исследования_клинико_диагностической_лаборатории:
					{ controller: function () { return c_myr_diagnostic_methods(x_myr_diagnostic_methods(h_myr_laboratory_description)); } }
				, Аудиологические_исследования:
					{ controller: function () { return c_myr_diagnostic_methods(x_myr_diagnostic_methods(h_myr_audiological_description)); } }
				, Кабинет_эндоскопии:
					{ controller: function () { return c_myr_diagnostic_methods(x_myr_diagnostic_methods(h_myr_endoscopy_description)); } }
			}
		};

		var controller = c_nailed(tpl, options);
		return controller;
	}
});
