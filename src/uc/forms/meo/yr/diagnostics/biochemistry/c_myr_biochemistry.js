define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/diagnostics/biochemistry/e_myr_biochemistry.html'
	, 'forms/meo/yr/diagnostics/methods/x_myr_diagnostic_methods'
	, 'forms/meo/yr/diagnostics/biochemistry/h_myr_biochemistry_description'
	, 'forms/meo/yr/diagnostics/methods/c_myr_diagnostic_methods'
],
function (c_nailed, tpl, x_myr_diagnostic_methods, h_myr_biochemistry_description, c_myr_diagnostic_methods)
{
	return function()
	{
		var options = {
			field_spec:
			{
				Биохимические_исследования: { controller: function () { return c_myr_diagnostic_methods(x_myr_diagnostic_methods(h_myr_biochemistry_description)); } }
			}
		};

		var controller = c_nailed(tpl, options);
		return controller;
	}
});
