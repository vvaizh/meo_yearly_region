start_store_lines_as biochemistry_fields_1

  je wbt.Push_workspace(' [model-selector="Явно_перечисленные_в_бланке"]');

  je wbt.Push_workspace(' [array-index="0"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", "checked");
  js wbt.SetModelFieldValue("Применяемый.ППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ПГ", null);
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="1"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ППГ", "checked");
  js wbt.SetModelFieldValue("Применяемый.ПГ", null);
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="2"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ПГ", "checked");
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="3"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", "checked");
  js wbt.SetModelFieldValue("Применяемый.ППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ПГ", null);
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="4"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ППГ", "checked");
  js wbt.SetModelFieldValue("Применяемый.ПГ", null);
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="5"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ПГ", "checked");
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="6"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", "checked");
  js wbt.SetModelFieldValue("Применяемый.ППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ПГ", null);
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="7"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ППГ", "checked");
  js wbt.SetModelFieldValue("Применяемый.ПГ", null);
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="8"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ПГ", "checked");
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="9"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", "checked");
  js wbt.SetModelFieldValue("Применяемый.ППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ПГ", null);
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="10"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ППГ", "checked");
  js wbt.SetModelFieldValue("Применяемый.ПГ", null);
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="11"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ПГ", "checked");
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="12"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", "checked");
  js wbt.SetModelFieldValue("Применяемый.ППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ПГ", null);
  je wbt.Pop_workspace();

  je wbt.Pop_workspace();

stop_store_lines

start_store_lines_as biochemistry_fields_2

  je wbt.Push_workspace(' [model-selector="Явно_перечисленные_в_бланке"]');

  je wbt.Push_workspace(' [array-index="0"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ППГ", "checked");
  js wbt.SetModelFieldValue("Применяемый.ПГ", null);
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="1"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ПГ", "checked");
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="2"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", "checked");
  js wbt.SetModelFieldValue("Применяемый.ППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ПГ", null);
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="3"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ППГ", "checked");
  js wbt.SetModelFieldValue("Применяемый.ПГ", null);
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="4"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ПГ", "checked");
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="5"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", "checked");
  js wbt.SetModelFieldValue("Применяемый.ППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ПГ", null);
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="6"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ППГ", "checked");
  js wbt.SetModelFieldValue("Применяемый.ПГ", null);
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="7"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ПГ", "checked");
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="8"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", "checked");
  js wbt.SetModelFieldValue("Применяемый.ППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ПГ", null);
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="9"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ППГ", "checked");
  js wbt.SetModelFieldValue("Применяемый.ПГ", null);
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="10"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ПГ", "checked");
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="11"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", "checked");
  js wbt.SetModelFieldValue("Применяемый.ППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ПГ", null);
  je wbt.Pop_workspace();

  je wbt.Push_workspace(' [array-index="12"]');
  js wbt.SetModelFieldValue("Применяемый.ПППГ", null);
  js wbt.SetModelFieldValue("Применяемый.ППГ", "checked");
  js wbt.SetModelFieldValue("Применяемый.ПГ", null);
  je wbt.Pop_workspace();

  je wbt.Pop_workspace();
stop_store_lines


