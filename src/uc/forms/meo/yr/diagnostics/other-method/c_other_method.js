define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/diagnostics/other-method/e_other_method.html'

],
function (c_nailed, tpl)
{
	return function(options)
	{
		var controller = c_nailed(tpl, options);

		controller.size = { width: 820, height: 345 };

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);
			$(sel + ' [model-selector="Метод"]').select().focus();
		}

		return controller;
	}
});
