define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/diagnostics/blood/e_myr_blood.html'
	, 'forms/meo/yr/diagnostics/methods/x_myr_diagnostic_methods'
	, 'forms/meo/yr/diagnostics/methods/c_myr_diagnostic_methods'
	, 'forms/meo/yr/diagnostics/blood/h_myr_hematology_description'
	, 'forms/meo/yr/diagnostics/blood/h_myr_coagulating_description'
],
function (c_nailed, tpl, x_myr_diagnostic_methods, c_myr_diagnostic_methods, h_myr_hematology_description, h_myr_coagulating_description)
{
	return function()
	{
		var options = {
			field_spec:
			{
				Гематологические_исследования:
					{ controller: function () { return c_myr_diagnostic_methods(x_myr_diagnostic_methods(h_myr_hematology_description)); } }
				, Исследования_свертывающей_системы_и_фибринолиза:
					{ controller: function () { return c_myr_diagnostic_methods(x_myr_diagnostic_methods(h_myr_coagulating_description)); } }
			}
		};

		var controller = c_nailed(tpl, options);
		return controller;
	}
});
