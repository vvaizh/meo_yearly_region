define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/economic/service/e_myr_service.html'
	, 'forms/meo/yr/economic/repair/c_myr_repair'
	, 'forms/base/h_msgbox'
	, 'forms/base/nailing/fastening/h_fastening_clip'
	, 'tpl!forms/meo/yr/economic/repair/v_myr_repair_confirm_delete.html'
],
function (c_nailed, tpl, c_myr_repair, h_msgbox, h_fastening_clip, v_myr_repair_confirm_delete)
{
	return function()
	{
		var controller = c_nailed(tpl);

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' div[model-selector="Ремонт"] button.add').click(function () { self.OnAddRepair(); });
			this.StaticOnEditRepair = function (e) { e.preventDefault(); self.OnEditRepair(e); }
			this.StaticOnDeleteRepair = function (e) { e.preventDefault(); self.OnDeleteRepair(e); }
			this.BindEventHandlers();
		}

		controller.BindEventHandlers = function ()
		{
			var self = this;
			var sel = this.fastening.selector;
			var sel_Repair= sel + ' div[model-selector="Ремонт"]';
			$(sel_Repair + ' span.text').off('click', this.StaticOnEditRepair).on('click', this.StaticOnEditRepair);
			$(sel_Repair + ' img.edit').off('click', this.StaticOnEditRepair).on('click', this.StaticOnEditRepair);
			$(sel_Repair + ' img.delete').off('click', this.StaticOnDeleteRepair).on('click', this.StaticOnDeleteRepair);
		}

		controller.OnAddRepair= function()
		{
			var sel = this.fastening.selector;
			var self = this;
			var c_item = c_myr_repair();
			c_item.SetFormContent({ Износ_процентов: "0" });
			var btnOk = 'Сохранить информацию о ремонте объекта в составе зданий/сооружений';
			h_msgbox.ShowModal
			({
				title: 'Добавление информации о ремонте объекта в составе зданий/сооружений'
				, controller: c_item
				, buttons: ['Отмена', btnOk]
				, id_div: "cpw-form-myr-repair"
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						var fc_dom_item = $(sel + ' div[model-selector="Ремонт"]');
						var model = self.fastening.get_fc_model_value(fc_dom_item);
						if (null == model)
							model = [];
						model.push(c_item.GetFormContent());
						self.fastening.set_fc_model_value(fc_dom_item, model);
						self.BindEventHandlers();
					}
				}
			});
		}

		controller.OnEditRepair= function(e)
		{
			var fastening = this.fastening;
			var sel = fastening.selector;
			var i_item = h_fastening_clip.get_parent_fc_of_type($(e.target), 'array-item').attr('array-index');
			var fc_dom_item = $(sel + ' div[model-selector="Ремонт"]');
			var model = fastening.get_fc_model_value(fc_dom_item);
			var c_item = c_myr_repair();
			c_item.SetFormContent(model[i_item]);
			var btnOk = 'Сохранить информацию о ремонте объекта в составе зданий/сооружений';
			var self = this;
			h_msgbox.ShowModal
			({
				title: 'Редактирование информации о ремонте объекта в составе зданий/сооружений'
				, controller: c_item
				, buttons: ['Отмена', btnOk]
				, id_div: "cpw-form-myr-repair"
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						var model = self.fastening.get_fc_model_value(fc_dom_item);
						model[i_item] = c_item.GetFormContent();
						fastening.set_fc_model_value(fc_dom_item, model);
						self.BindEventHandlers();
					}
				}
			});
		}

		controller.OnDeleteRepair = function (e)
		{
			var fastening = this.fastening;
			var sel = fastening.selector;
			var i_item = h_fastening_clip.get_parent_fc_of_type($(e.target), 'array-item').attr('array-index');
			var fc_dom_item = $(sel + ' div[model-selector="Ремонт"]');
			var model = fastening.get_fc_model_value(fc_dom_item);
			var self = this;

			var btnOk = 'Да, удалить';
			h_msgbox.ShowModal({
				title: 'Подтверждение удаления информации о ремонте объекта в составе зданий/сооружений', width: 700, height: 260
				, html: v_myr_repair_confirm_delete({ Удаляемый_ремонт: model[i_item] })
				, buttons: ['Отмена', btnOk]
				, id_div: "cpw-form-myr-repair-delete-confirm"
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						model.splice(i_item, 1);
						self.fastening.set_fc_model_value(fc_dom_item, model);
						self.BindEventHandlers();
					}
				}
			});
		}

		return controller;
	}
});
