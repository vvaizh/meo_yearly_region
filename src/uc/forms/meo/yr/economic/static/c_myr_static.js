define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/economic/static/e_myr_static.html'
],
function (c_nailed, tpl)
{
	return function()
	{
		var controller = c_nailed(tpl);

		return controller;
	}
});
