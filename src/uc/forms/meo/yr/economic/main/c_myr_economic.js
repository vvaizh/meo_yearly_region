define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/economic/main/e_myr_economic.html'
	, 'forms/meo/yr/economic/static/c_myr_static'
	, 'forms/meo/yr/economic/procurement/c_myr_procurement'
	, 'forms/meo/yr/economic/service/c_myr_service'
],
function (c_nailed, tpl, c_myr_static, c_myr_procurement, c_myr_service)
{
	return function()
	{
		var options = {
			field_spec:
				{
					Статические: { controller: c_myr_static }
					, Закупки: { controller: c_myr_procurement }
					, Обслуживание: { controller: c_myr_service }
				}
		};

		var controller = c_nailed(tpl, options);

		return controller;
	}
});
