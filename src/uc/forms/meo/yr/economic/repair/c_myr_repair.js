define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/economic/repair/e_myr_repair.html'
],
function (c_nailed, tpl)
{
	return function()
	{
		var controller = c_nailed(tpl);

		controller.size = { width: 800, height: 300 };

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			$(sel + ' [model-selector="Объект"]').select().focus();
		}

		return controller;
	}
});
