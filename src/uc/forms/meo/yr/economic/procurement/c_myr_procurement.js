define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/economic/procurement/e_myr_procurement.html'
],
function (c_nailed, tpl)
{
	return function()
	{
		var controller = c_nailed(tpl);

		return controller;
	}
});
