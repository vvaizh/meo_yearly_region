start_store_lines_as economic_procurement_fields_1

  js wbt.SetModelFieldValue("Оборудование.За_бюджет.План.ПППГ", "1");
  js wbt.SetModelFieldValue("Оборудование.За_бюджет.Факт.ПППГ", "2");
  js wbt.SetModelFieldValue("Оборудование.За_бюджет.План.ППГ", "3");
  js wbt.SetModelFieldValue("Оборудование.За_бюджет.Факт.ППГ", "4");
  js wbt.SetModelFieldValue("Оборудование.За_бюджет.План.ПГ", "5");
  js wbt.SetModelFieldValue("Оборудование.За_бюджет.Факт.ПГ", "6");

  js wbt.SetModelFieldValue("Оборудование.За_внебюджет.План.ПППГ", "7");
  js wbt.SetModelFieldValue("Оборудование.За_внебюджет.Факт.ПППГ", "8");
  js wbt.SetModelFieldValue("Оборудование.За_внебюджет.План.ППГ", "9");
  js wbt.SetModelFieldValue("Оборудование.За_внебюджет.Факт.ППГ", "10");
  js wbt.SetModelFieldValue("Оборудование.За_внебюджет.План.ПГ", "11");
  js wbt.SetModelFieldValue("Оборудование.За_внебюджет.Факт.ПГ", "12");

  js wbt.SetModelFieldValue("Расходники.За_бюджет.План.ПППГ", "13");
  js wbt.SetModelFieldValue("Расходники.За_бюджет.Факт.ПППГ", "14");
  js wbt.SetModelFieldValue("Расходники.За_бюджет.План.ППГ", "15");
  js wbt.SetModelFieldValue("Расходники.За_бюджет.Факт.ППГ", "16");
  js wbt.SetModelFieldValue("Расходники.За_бюджет.План.ПГ", "17");
  js wbt.SetModelFieldValue("Расходники.За_бюджет.Факт.ПГ", "18");

  js wbt.SetModelFieldValue("Расходники.За_внебюджет.План.ПППГ", "19");
  js wbt.SetModelFieldValue("Расходники.За_внебюджет.Факт.ПППГ", "20");
  js wbt.SetModelFieldValue("Расходники.За_внебюджет.План.ППГ", "21");
  js wbt.SetModelFieldValue("Расходники.За_внебюджет.Факт.ППГ", "22");
  js wbt.SetModelFieldValue("Расходники.За_внебюджет.План.ПГ", "23");
  js wbt.SetModelFieldValue("Расходники.За_внебюджет.Факт.ПГ", "24");

  js wbt.SetModelFieldValue("Закупки.Оборудование.ПППГ", "25 да покупали");
  js wbt.SetModelFieldValue("Закупки.Оборудование.ППГ", "26 нет не покупали");
  js wbt.SetModelFieldValue("Закупки.Оборудование.ПГ", "27 держимся на старом");

  js wbt.SetModelFieldValue("Закупки.Расходники.ПППГ", "28 да закупали");
  js wbt.SetModelFieldValue("Закупки.Расходники.ППГ", "29 нет не закупали");
  js wbt.SetModelFieldValue("Закупки.Расходники.ПГ", "30 доедаем последнее");

stop_store_lines

start_store_lines_as economic_procurement_fields_2
  js wbt.SetModelFieldValue("Оборудование.За_бюджет.План.ПППГ", "11");
  js wbt.SetModelFieldValue("Оборудование.За_бюджет.Факт.ПППГ", "12");
  js wbt.SetModelFieldValue("Оборудование.За_бюджет.План.ППГ", "13");
  js wbt.SetModelFieldValue("Оборудование.За_бюджет.Факт.ППГ", "14");
  js wbt.SetModelFieldValue("Оборудование.За_бюджет.План.ПГ", "15");
  js wbt.SetModelFieldValue("Оборудование.За_бюджет.Факт.ПГ", "16");

  js wbt.SetModelFieldValue("Оборудование.За_внебюджет.План.ПППГ", "17");
  js wbt.SetModelFieldValue("Оборудование.За_внебюджет.Факт.ПППГ", "18");
  js wbt.SetModelFieldValue("Оборудование.За_внебюджет.План.ППГ", "19");
  js wbt.SetModelFieldValue("Оборудование.За_внебюджет.Факт.ППГ", "110");
  js wbt.SetModelFieldValue("Оборудование.За_внебюджет.План.ПГ", "111");
  js wbt.SetModelFieldValue("Оборудование.За_внебюджет.Факт.ПГ", "112");

  js wbt.SetModelFieldValue("Расходники.За_бюджет.План.ПППГ", "113");
  js wbt.SetModelFieldValue("Расходники.За_бюджет.Факт.ПППГ", "114");
  js wbt.SetModelFieldValue("Расходники.За_бюджет.План.ППГ", "115");
  js wbt.SetModelFieldValue("Расходники.За_бюджет.Факт.ППГ", "116");
  js wbt.SetModelFieldValue("Расходники.За_бюджет.План.ПГ", "117");
  js wbt.SetModelFieldValue("Расходники.За_бюджет.Факт.ПГ", "118");

  js wbt.SetModelFieldValue("Расходники.За_внебюджет.План.ПППГ", "119");
  js wbt.SetModelFieldValue("Расходники.За_внебюджет.Факт.ПППГ", "120");
  js wbt.SetModelFieldValue("Расходники.За_внебюджет.План.ППГ", "121");
  js wbt.SetModelFieldValue("Расходники.За_внебюджет.Факт.ППГ", "122");
  js wbt.SetModelFieldValue("Расходники.За_внебюджет.План.ПГ", "123");
  js wbt.SetModelFieldValue("Расходники.За_внебюджет.Факт.ПГ", "124");

  js wbt.SetModelFieldValue("Закупки.Оборудование.ПППГ", "125");
  js wbt.SetModelFieldValue("Закупки.Оборудование.ППГ", "126");
  js wbt.SetModelFieldValue("Закупки.Оборудование.ПГ", "127");

  js wbt.SetModelFieldValue("Закупки.Расходники.ПППГ", "128");
  js wbt.SetModelFieldValue("Закупки.Расходники.ППГ", "129");
  js wbt.SetModelFieldValue("Закупки.Расходники.ПГ", "130");
stop_store_lines
