define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/organizational/audience/e_myr_audience.html'
	, 'forms/meo/yr/h_regions'
],
function (c_nailed, tpl, h_regions)
{
	return function()
	{
		var options =
		{
			field_spec:
			{
				'Субъект_РФ':
				{
					placeholder: 'Выберите регион',
					allowClear: true,
					query: function (q)
					{
						q.callback({ results: h_regions.FindForSelect2(q.term) });
					}
				}
			}
		};

		var controller = c_nailed(tpl, options);

		return controller;
	}
});
