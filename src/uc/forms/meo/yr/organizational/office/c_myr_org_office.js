define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/organizational/office/e_myr_org_office.html'

],
function (c_nailed, tpl)
{
	return function(options)
	{
		var controller = c_nailed(tpl, options);

		controller.size = { width: 985, height: 260 };

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);
			$(sel + ' [model-selector="Наименование"]').select().focus();
		}

		return controller;
	}
});
