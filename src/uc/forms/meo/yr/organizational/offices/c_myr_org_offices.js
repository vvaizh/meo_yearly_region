define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/organizational/offices/e_myr_org_offices.html'
	, 'forms/meo/yr/organizational/office/c_myr_org_office'
	, 'forms/base/h_msgbox'
	, 'forms/base/nailing/fastening/h_fastening_clip'
	, 'tpl!forms/meo/yr/organizational/office/v_myr_org_office_confirm_delete.html'
],
function (c_nailed, tpl, c_myr_org_office, h_msgbox, h_fastening_clip, v_org_office_confirm_delete)
{
	return function()
	{
		var controller = c_nailed(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' button.add').click(function () { self.OnAdd(); });
			this.StaticOnEdit = function (e) { e.preventDefault(); self.OnEdit(e); }
			this.StaticOnDelete = function (e) { e.preventDefault(); self.OnDelete(e); }
			this.StaticOnChangeTextbox = function (e) { self.OnChangeTextbox(e.target); };
			this.BindEventHandlers();
			this.CalculateTotal();
		}

		controller.BindEventHandlers = function ()
		{
			var sel = this.fastening.selector;
			var self = this;
			$(sel + ' td.office-name span').off('click', this.StaticOnEdit).on('click', this.StaticOnEdit);
			$(sel + ' td.edit img').off('click', this.StaticOnEdit).on('click', this.StaticOnEdit);
			$(sel + ' td.delete img').off('click', this.StaticOnDelete).on('click', this.StaticOnDelete);
			$(sel + ' input').off('change', this.StaticOnChangeTextbox).on('change', this.StaticOnChangeTextbox);
		}

		controller.OnChangeTextbox = function () { }

		controller.OnAdd = function ()
		{
			var sel = this.fastening.selector;
			var self = this;
			var c_item = c_myr_org_office();
			c_item.SetFormContent({});
			var btnOk = 'Сохранить информацию о профпатологическом отделении';
			h_msgbox.ShowModal
			({
				title: 'Добавление информации о профпатологическом отделении'
				, controller: c_item
				, buttons: ['Отмена', btnOk]
				, id_div: "cpw-form-myr-org-office"
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						var fc_dom_item = $(self.fastening.selector);
						var model = self.fastening.get_fc_model_value(fc_dom_item);
						if (null == model)
							model = [];
						model.push(c_item.GetFormContent());
						self.fastening.set_fc_model_value(fc_dom_item, model);
						self.BindEventHandlers();
						self.CalculateTotal();
					}
				}
			});
		}

		controller.OnEdit = function (e)
		{
			var fastening = this.fastening;
			var i_item = h_fastening_clip.get_parent_fc_of_type($(e.target), 'array-item').attr('array-index');
			var fc_dom_item = $(this.fastening.selector);
			var model = fastening.get_fc_model_value(fc_dom_item);
			var c_item = c_myr_org_office();
			c_item.SetFormContent(model[i_item]);
			var btnOk = 'Сохранить информацию о профпатологическом отделении';
			var self = this;
			h_msgbox.ShowModal
			({
				title: 'Редактирование информации о профпатологическом отделении'
				, controller: c_item
				, buttons: ['Отмена', btnOk]
				, id_div: "cpw-form-myr-org-office"
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						var fc_dom_item = $(self.fastening.selector);
						var model = self.fastening.get_fc_model_value(fc_dom_item);
						model[i_item] = c_item.GetFormContent();
						self.fastening.set_fc_model_value(fc_dom_item, model);
						self.BindEventHandlers();
						self.CalculateTotal();
					}
				}
			});
		}

		controller.OnDelete = function (e)
		{
			var fastening = this.fastening;
			var i_item = h_fastening_clip.get_parent_fc_of_type($(e.target), 'array-item').attr('array-index');
			var fc_dom_item = $(this.fastening.selector);
			var model = fastening.get_fc_model_value(fc_dom_item);
			var self = this;

			var btnOk = 'Да, удалить';
			h_msgbox.ShowModal({
				title: 'Подтверждение удаления информации о профпатологическом отделении', width: 680, height: 260
				, html: v_org_office_confirm_delete({ Удаляемое_отделение: model[i_item] })
				, buttons: ['Отмена', btnOk]
				, id_div: "cpw-form-myr-org-office-delete-confirm"
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						model.splice(i_item, 1);
						self.fastening.set_fc_model_value(fc_dom_item, model);
						self.BindEventHandlers();
						self.CalculateTotal();
					}
				}
			});
		}

		controller.CalculateTotal= function()
		{
			var sel = this.fastening.selector;
			var AddCots = function ()
			{
				var v = $(this).val();
				if ('' != v)
				{
					var iv = parseInt(v);
					if (!isNaN(iv))
					{
						tcots += iv;
						tprofile++;
					}
				}
			}
			var tprofile = 0, tcots = 0;
			$(sel + ' input[model-selector="Коек.ПППГ"]').each(AddCots);
			$(sel + ' .tprofile0').text(tprofile);
			$(sel + ' .tcots0').text(tcots);
			tprofile = 0, tcots = 0;
			$(sel + ' input[model-selector="Коек.ППГ"]').each(AddCots);
			$(sel + ' .tprofile1').text(tprofile);
			$(sel + ' .tcots1').text(tcots);
			tprofile = 0, tcots = 0;
			$(sel + ' input[model-selector="Коек.ПГ"]').each(AddCots);
			$(sel + ' .tprofile2').text(tprofile);
			$(sel + ' .tcots2').text(tcots);
		}

		return controller;
	}
});
