define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/organizational/unit/e_myr_org_unit.html'

],
function (c_nailed, tpl)
{
	return function(options)
	{
		var controller = c_nailed(tpl, options);

		controller.size = { width: 885, height: 460 };

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);
			$(sel + ' [model-selector="Наименование"]').select().focus();

			var self = this;
			$(sel + ' tr.item').change(function () { self.RecalcTotal(); });
		}

		controller.RecalcTotal= function()
		{
			var sel = this.fastening.selector;
			var categories_to_sum = ['Заведующих', 'Врачей', 'Ср_медперсонал', 'Мл_медперсонал', 'Прочий_персонал'];

			var get_num =function(sel_postfix)
			{
				var v = $(sel + ' [model-selector="' + sel_postfix).val();
				if ('' != v)
				{
					var iv = parseInt(v);
					if (!isNaN(iv))
						return iv;
				}
				return 0;
			}

			var total = { Должностей: { ПППГ: 0, ППГ: 0, ПГ: 0 }, Занято_должностей: { ПППГ: 0, ППГ: 0, ПГ: 0 }, Физ_лиц: { ПППГ: 0, ППГ: 0, ПГ: 0 } };
			for (var i = 0; i < categories_to_sum.length; i++)
			{
				var category_name = categories_to_sum[i];
				
				total.Должностей.ПППГ += get_num(category_name + '.Должностей.ПППГ"]');
				total.Должностей.ППГ += get_num(category_name + '.Должностей.ППГ"]');
				total.Должностей.ПГ += get_num(category_name + '.Должностей.ПГ"]');

				total.Занято_должностей.ПППГ += get_num(category_name + '.Занято_должностей.ПППГ"]');
				total.Занято_должностей.ППГ += get_num(category_name + '.Занято_должностей.ППГ"]');
				total.Занято_должностей.ПГ += get_num(category_name + '.Занято_должностей.ПГ"]');

				total.Физ_лиц.ПППГ += get_num(category_name + '.Физ_лиц.ПППГ"]');
				total.Физ_лиц.ППГ += get_num(category_name + '.Физ_лиц.ППГ"]');
				total.Физ_лиц.ПГ += get_num(category_name + '.Физ_лиц.ПГ"]');
			}

			 $(sel + ' [model-selector="Итого.Должностей.ПППГ"]').val(total.Должностей.ПППГ);
			 $(sel + ' [model-selector="Итого.Должностей.ППГ"]').val(total.Должностей.ППГ);
			 $(sel + ' [model-selector="Итого.Должностей.ПГ"]').val(total.Должностей.ПГ);

			 $(sel + ' [model-selector="Итого.Занято_должностей.ПППГ"]').val(total.Занято_должностей.ПППГ);
			 $(sel + ' [model-selector="Итого.Занято_должностей.ППГ"]').val(total.Занято_должностей.ППГ);
			 $(sel + ' [model-selector="Итого.Занято_должностей.ПГ"]').val(total.Занято_должностей.ПГ);

			 $(sel + ' [model-selector="Итого.Физ_лиц.ПППГ"]').val(total.Физ_лиц.ПППГ);
			 $(sel + ' [model-selector="Итого.Физ_лиц.ППГ"]').val(total.Физ_лиц.ППГ);
			 $(sel + ' [model-selector="Итого.Физ_лиц.ПГ"]').val(total.Физ_лиц.ПГ);
		}

		return controller;
	}
});
