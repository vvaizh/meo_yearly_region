define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/organizational/licenses/e_myr_licenses.html'
],
function (c_nailed, tpl)
{
	return function()
	{
		var controller = c_nailed(tpl);

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' input[type="checkbox"]').change(function (e) { self.OnChangeCheckbox(e.target); });
			$(sel + ' input[type="checkbox"]').each(function () { self.OnChangeCheckbox(this); });
		}

		controller.OnChangeCheckbox= function(aitem)
		{
			var item = $(aitem);
			var label = item.parent('label')
			label.removeClass('no');
			label.removeClass('yes');
			label.addClass('checked'==item.attr('checked')?'yes':'no');
		}

		return controller;
	}
});
