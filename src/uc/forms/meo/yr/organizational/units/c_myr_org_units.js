define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/organizational/units/e_myr_org_units.html'
	, 'forms/meo/yr/organizational/unit/c_myr_org_unit'
	, 'forms/base/h_msgbox'
	, 'forms/base/nailing/fastening/h_fastening_clip'
	, 'tpl!forms/meo/yr/organizational/unit/v_myr_org_unit_confirm_delete.html'
	, 'forms/base/codec/codec.copy'
],
function (c_nailed, tpl, c_myr_org_unit, h_msgbox, h_fastening_clip, v_org_unit_confirm_delete, codec_copy)
{
	return function()
	{
		var controller = c_nailed(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' button.add').click(function () { self.OnAdd(); });
			this.StaticOnEdit = function (e) { e.preventDefault(); self.OnEdit(e); }
			this.StaticOnDelete = function (e) { e.preventDefault(); self.OnDelete(e); }
			this.StaticOnChangeTextbox = function (e) { self.OnChangeTextbox(e.target); };
			this.BindEventHandlers();
			this.CalculateTotal();
		}

		controller.BindEventHandlers = function ()
		{
			var sel = this.fastening.selector;
			var self = this;
			$(sel + ' td.unit-name span').off('click', this.StaticOnEdit).on('click', this.StaticOnEdit);
			$(sel + ' td.edit img').off('click', this.StaticOnEdit).on('click', this.StaticOnEdit);
			$(sel + ' td.delete img').off('click', this.StaticOnDelete).on('click', this.StaticOnDelete);
			$(sel + ' input').off('change', this.StaticOnChangeTextbox).on('change', this.StaticOnChangeTextbox);
		}

		controller.OnChangeTextbox = function () { }

		controller.OnAdd = function ()
		{
			var sel = this.fastening.selector;
			var self = this;
			var c_item = c_myr_org_unit();
			c_item.SetFormContent({});
			var btnOk = 'Сохранить информацию о подразделении';
			h_msgbox.ShowModal
			({
				title: 'Добавление информации о подразделении'
				, controller: c_item
				, buttons: ['Отмена', btnOk]
				, id_div: "cpw-form-myr-org-unit"
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						var fc_dom_item = $(sel + ' [model-selector="Подразделения"]');
						var model = self.fastening.get_fc_model_value(fc_dom_item);
						if (null == model)
							model = [];
						model.push(c_item.GetFormContent());
						self.fastening.set_fc_model_value(fc_dom_item, model);
						self.BindEventHandlers();
						self.CalculateTotal();
					}
				}
			});
		}

		controller.OnEdit = function (e)
		{
			var fastening = this.fastening;
			var sel = fastening.selector;
			var i_item = h_fastening_clip.get_parent_fc_of_type($(e.target), 'array-item').attr('array-index');
			var fc_dom_item = $(sel + ' [model-selector="Подразделения"]');
			var model = fastening.get_fc_model_value(fc_dom_item);
			var c_item = c_myr_org_unit();
			c_item.SetFormContent(model[i_item]);
			var btnOk = 'Сохранить информацию о подразделении';
			var self = this;
			h_msgbox.ShowModal
			({
				title: 'Редактирование информации о подразделении'
				, controller: c_item
				, buttons: ['Отмена', btnOk]
				, id_div: "cpw-form-myr-org-unit"
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						var model = self.fastening.get_fc_model_value(fc_dom_item);
						model[i_item] = c_item.GetFormContent();
						self.fastening.set_fc_model_value(fc_dom_item, model);
						self.BindEventHandlers();
						self.CalculateTotal();
					}
				}
			});
		}

		controller.OnDelete = function (e)
		{
			var fastening = this.fastening;
			var sel = fastening.selector;
			var i_item = h_fastening_clip.get_parent_fc_of_type($(e.target), 'array-item').attr('array-index');
			var fc_dom_item = $(sel + ' [model-selector="Подразделения"]');
			var model = fastening.get_fc_model_value(fc_dom_item);
			var self = this;

			var btnOk = 'Да, удалить';
			h_msgbox.ShowModal({
				title: 'Подтверждение удаления информации о подразделении', width: 680, height: 260
				, html: v_org_unit_confirm_delete({ Удаляемое_подразделение: model[i_item] })
				, buttons: ['Отмена', btnOk]
				, id_div: "cpw-form-myr-org-unit-delete-confirm"
				, onclose: function (btn)
				{
					if (btn == btnOk)
					{
						model.splice(i_item, 1);
						self.fastening.set_fc_model_value(fc_dom_item, model);
						self.BindEventHandlers();
						self.CalculateTotal();
					}
				}
			});
		}

		var CreateEmptyTotal= function()
		{
			var c= codec_copy();
			var n = { ПППГ: 0, ППГ: 0, ПГ: 0 };
			var category = { Должностей: c.Copy(n), Занято_должностей: c.Copy(n), Физ_лиц: c.Copy(n) }
			return { 
				Врачи: c.Copy(category)
				, Ср_медперсонал: c.Copy(category)
				, Мл_медперсонал: c.Copy(category)
				, Прочий_персонал: c.Copy(category)
				, Итого: c.Copy(category) 
			};
		}

		var Add= function(t, v)
		{
			if ('' != v)
			{
				var iv = parseInt(v);
				if (!isNaN(iv))
				{
					t += iv;
				}
			}
			return t;
		}

		var AddTotalYears= function(t, item)
		{
			if (t && item)
			{
				t.ПППГ = Add(t.ПППГ, item.ПППГ);
				t.ППГ = Add(t.ППГ, item.ППГ);
				t.ПГ = Add(t.ПГ, item.ПГ);
			}
		}

		var AddTotalCategory= function(t, item)
		{
			if (t && item)
			{
				AddTotalYears(t.Должностей, item.Должностей);
				AddTotalYears(t.Занято_должностей, item.Занято_должностей);
				AddTotalYears(t.Физ_лиц, item.Физ_лиц);
			}
		}

		var AddTotal= function(t, item)
		{
			AddTotalCategory(t.Врачи, item.Врачей);
			AddTotalCategory(t.Ср_медперсонал, item.Ср_медперсонал);
			AddTotalCategory(t.Мл_медперсонал, item.Мл_медперсонал);
			AddTotalCategory(t.Прочий_персонал, item.Прочий_персонал);

			AddTotalCategory(t.Итого, item.Врачей);
			AddTotalCategory(t.Итого, item.Ср_медперсонал);
			AddTotalCategory(t.Итого, item.Мл_медперсонал);
			AddTotalCategory(t.Итого, item.Прочий_персонал);
		}

		controller.CalculateTotal= function()
		{
			var fastening = this.fastening;
			var sel = fastening.selector;
			var t = CreateEmptyTotal();
			var fc_dom_item = $(sel + ' [model-selector="Подразделения"]');
			var Подразделения = fastening.get_fc_model_value(fc_dom_item);
			if (Подразделения)
			{
				for (var i = 0; i < Подразделения.length; i++)
					AddTotal(t, Подразделения[i]);
			}
			var fc_dom_item = $(sel + ' [model-selector="Суммарно"]');
			fastening.set_fc_model_value(fc_dom_item, t);
		}

		return controller;
	}
});
