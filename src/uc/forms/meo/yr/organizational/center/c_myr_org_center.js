define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/organizational/center/e_myr_org_center.html'
],
function (c_nailed, tpl)
{
	return function()
	{
		var controller = c_nailed(tpl);

		return controller;
	}
});
