define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/organizational/main/e_myr_organizational.html'

	, 'forms/meo/yr/organizational/center/c_myr_org_center'
	, 'forms/meo/yr/organizational/audience/c_myr_audience'
	, 'forms/meo/yr/organizational/offices/c_myr_org_offices'
	, 'forms/meo/yr/organizational/licenses/c_myr_licenses'
	, 'forms/meo/yr/organizational/units/c_myr_org_units'
],
function (c_nailed, tpl

	, c_myr_org_center
	, c_myr_audience
	, c_myr_org_offices
	, c_myr_licenses
	, c_myr_org_units
	)
{
	return function()
	{
		var options = {
			field_spec:
				{
					Центр: { controller: c_myr_org_center }
					, Аудитория: { controller: c_myr_audience }
					, Койко_места: { controller: c_myr_org_offices }
					, Лицензии_и_комиссии: { controller: c_myr_licenses }
					, Подразделения: { controller: c_myr_org_units }
				}
		};

		var controller = c_nailed(tpl, options);

		return controller;
	}
});
