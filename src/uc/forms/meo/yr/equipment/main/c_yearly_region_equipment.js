define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/equipment/main/e_yearly_region_equipment.html'

	, 'forms/meo/yr/equipment/center/c_yearly_region_equipment_center'
	, 'forms/meo/yr/equipment/partition/c_yearly_region_equipment_partition'
	, 'forms/meo/yr/equipment/cabinet/c_yearly_region_equipment_cabinet'
],
function (c_nailed, tpl

	, c_yearly_region_equipment_center
	, c_yearly_region_equipment_partition
	, c_yearly_region_equipment_cabinet
	)
{
	return function()
	{
		var options = {
			field_spec:
				{
					Центр: { controller: c_yearly_region_equipment_center }
					, Отделение: { controller: c_yearly_region_equipment_partition }
					, Кабинет: { controller: c_yearly_region_equipment_cabinet }
				}
		};

		var controller = c_nailed(tpl, options);

		return controller;
	}
});
