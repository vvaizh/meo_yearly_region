define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/equipment/partition/e_yearly_region_equipment_partition.html'

],
function (c_nailed, tpl)
{
	return function()
	{
		var controller = c_nailed(tpl);
		return controller;
	}
});
