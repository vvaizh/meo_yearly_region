require.config
({
	enforceDefine: true,
	urlArgs: "bust" + (new Date()).getTime(),
	baseUrl: '.',
	paths:
	{
		styles: 'css',
		images: 'images'
	},
	map:
	{
		'*':
		{
			tpl: 'js/libs/tpl',
			css: 'js/libs/css',
			img: 'js/libs/image',
			txt: 'js/libs/text'
		}
	}
}),

require
(
	[
		  'forms/meo/yr/main/f_yearly_region'
	],
	function ()
	{
		var extension =
		{
			Title: '��� �������� �����'
			, key: 'meo'
			, forms: {}
			, profile: {}
		};
		var forms = Array.prototype.slice.call(arguments, 0);
		for (var i = 0; i < forms.length; i++)
		{
			var form = arguments[i];
			extension.forms[form.key] = form;
		}
		if (RegisterCpwFormsExtension)
		{
			RegisterCpwFormsExtension(extension);
		}
		return extension;
	}
);