﻿define([
	  'forms/meo/yr/main/c_yearly_region_ce'
	, 'forms/meo/yr/main/ff_yearly_region'
],
function (CreateController, FileFormat)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'yearly_region'
		, Title: 'Ежегодный отчёт центра профпатологии'
		, FileFormat: FileFormat
	};
	return form_spec;
});
