define([
	  'forms/base/codec/xml/codec.xml'
	, 'forms/base/log'
	, 'forms/base/codec/codec.copy'
],
function (BaseCodec, GetLogger, codec_copy)
{
	var log = GetLogger('x_yearly_region');
	return function ()
	{
		log.Debug('Create {');
		var res = BaseCodec();

		var schemaDiagnosticMethods= function()
		{
			return { fields: { 'Прочие': { type: 'array', item: { tagName: 'Метод' } }, 'Явно_перечисленные_в_бланке': { type: 'array', item: { tagName: 'Метод' } } } };
		}

		res.schema =
		{
			tagName: 'Годовой_отчёт_регионального_профпатологического_центра_в_НИИ_Медицины_труда'
			, fields:
			{
				'Общее':
				{
					fields:
					{
						'Подразделения':
						{
							type: 'array'
							, item: { tagName: 'Подразделение' }
						}
						, 'Койко_места':
						{
							type: 'array'
							, item:
							{
								tagName: 'Отделение'
								, fields: { 'Отделение': { tagName: 'Наименование' } }
							}
						}
					}
				}
				,'Экономика':
				{
					fields:
					{
						'Обслуживание':
						{
							fields:
							{
								'Ремонт': { type: 'array', item: { tagName: 'Ремонт_объекта' } }
							}
						}
					}
				}
				,'Диагностика':
				{
					fields:
					{
						'Кабинет_эндоскопии': schemaDiagnosticMethods()
						, 'Аудиологические_исследования': schemaDiagnosticMethods()
						, 'Исследования_клинико_диагностической_лаборатории': schemaDiagnosticMethods()
						, 'Гормональные_исследования': schemaDiagnosticMethods()
						, 'Иммунологические_исследования': schemaDiagnosticMethods()
						, 'Отделение_функциональной_диагностики': schemaDiagnosticMethods()
						, 'Биофизические_исследования': schemaDiagnosticMethods()
						, 'Кабинет_ультразвуковой_диагностики': schemaDiagnosticMethods()
						, 'Рентгеновский_кабинет': schemaDiagnosticMethods()
						, 'Биохимические_исследования': schemaDiagnosticMethods()
						, 'Исследования_свертывающей_системы_и_фибринолиза': schemaDiagnosticMethods()
						, 'Гематологические_исследования': schemaDiagnosticMethods()
						, 'Общеклинические_исследования': schemaDiagnosticMethods()
					}
				}
			}
		};

		log.Debug('Create }');
		return res;
	}

});
