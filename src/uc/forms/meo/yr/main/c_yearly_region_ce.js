﻿define([
	  'forms/meo/yr/econtent/c_econtent'
	, 'forms/meo/yr/main/c_yearly_region'
	, 'forms/meo/yr/main/ff_yearly_region'
],
function (BaseController, BaseFormController, formSpec)
{
	return function ()
	{
		var controller = BaseController(BaseFormController, formSpec);

		controller.GetCoveringLetterText = function ()
		{
			return 'Направляем документ\r\n' +
				'   "Ежегодный отчета центра профпатологии в НИИ Медицины труда"';
		}

		return controller;
	}
});