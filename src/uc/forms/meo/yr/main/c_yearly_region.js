define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/main/e_yearly_region.html'
	, 'forms/meo/yr/main/x_yearly_region'
	, 'tpl!forms/meo/yr/main/v_yearly_region.xaml'
	, 'forms/base/codec/codec.tpl.xaml'

	, 'forms/meo/yr/organizational/main/c_myr_organizational'
	, 'forms/meo/yr/economic/main/c_myr_economic'
	, 'forms/meo/yr/staff/main/c_myr_staff'
	, 'forms/meo/yr/results/main/c_yearly_region_results'
	, 'forms/meo/yr/diagnostics/main/c_myr_diagnostic'
	, 'forms/meo/yr/equipment/main/c_yearly_region_equipment'
],
function (c_nailed, tpl, x_yearly_region, v_yearly_region, codec_tpl_xaml
	, c_myr_organizational
	, c_myr_economic
	, c_yearly_region_staff
	, c_yearly_region_results
	, c_myr_diagnostic
	, c_yearly_region_equipment
	)
{
	return function()
	{
		var options = {
			field_spec:
				{
					Общее: { controller: c_myr_organizational }
					, Экономика: { controller: c_myr_economic }
					, Кадры: { controller: c_yearly_region_staff }
					, Деятельность: { controller: c_yearly_region_results }
					, Диагностика: { controller: c_myr_diagnostic }
					, Оборудование: { controller: c_yearly_region_equipment }
				}
		};

		var controller = c_nailed(tpl, options);

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' button.next').click(function () { self.OnNavigate('next'); });
			$(sel + ' button.prev').click(function () { self.OnNavigate('prev'); });
			$(sel + ' div.ui-tabs').on('tabsactivate', function () { setTimeout(function () { self.UpdateButtonStates(); }, 200); });
			$(sel + ' div.ui-tabs').on('activate', function () { setTimeout(function () { self.UpdateButtonStates(); }, 200); });
		}

		controller.OnNavigate= function(direction)
		{
			var sub_next_li = this.get_active_page_li2()[direction]();
			if (0 != sub_next_li.length)
			{
				sub_next_li.children('a').click();
			}
			else
			{
				var next_li = this.get_active_page_li()[direction]();
				if (0 != next_li.length)
				{
					var next_li_a = next_li.children('a');
					
					var sel_id = next_li_a.attr('href');
					next_li_a.click();

					var sel = this.fastening.selector;
					var sel_tab = sel + ' div#cpw-meo-yearly-report-tabs.ui-tabs';
					var sel_a = sel_tab + ' div.page' + sel_id + ' div.ui-tabs li::' + ('next' == direction ? 'first-child' : 'last-child') + ' a';
					$(sel_a).click();
				}
			}
			this.UpdateButtonStates();
		}

		controller.get_active_page_li= function()
		{
			var sel = this.fastening.selector;
			var sel_tab = sel + ' div#cpw-meo-yearly-report-tabs.ui-tabs';
			var res = $(sel_tab + ' > ul > li.ui-tabs-selected');
			return 0!=res.length ? res : $(sel_tab + ' > ul > li[aria-selected="true"]');
		}

		controller.get_active_page_li2 = function ()
		{
			var sel = this.fastening.selector;
			var sel_tab = sel + ' div#cpw-meo-yearly-report-tabs.ui-tabs';
			var res = $(sel_tab + ' div.page:not(.ui-tabs-hide) div.ui-tabs li.ui-tabs-selected');
			return 0 != res.length ? res : $(sel_tab + ' div.page[aria-hidden="false"] div.ui-tabs li[aria-selected="true"]')
		}

		controller.ButtonState= function(direction)
		{
			var sub_next_li = this.get_active_page_li2()[direction]();
			if (0 == sub_next_li.length)
			{
				var next_li = this.get_active_page_li()[direction]();
				if (0 == next_li.length)
					return 'disabled'
			}
			return null;
		}

		controller.UpdateButtonStates= function()
		{
			var sel = this.fastening.selector;
			$(sel + ' button.next').attr('disabled', this.ButtonState('next'));
			$(sel + ' button.prev').attr('disabled', this.ButtonState('prev'));
		}

		controller.BuildXamlView = function ()
		{
			var model = !this.binding ? this.model : base_GetFormContent.call(this);
			var content = v_yearly_region({
				form: model
			});
			var codec = codec_tpl_xaml();
			return codec.Encode(content);
		}

		controller.UseCodec(x_yearly_region());

		return controller;
	}
});
