define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/results/chronic/e_myr_chronic.html'
],
function (c_nailed, tpl)
{
	return function()
	{
		var items_description = [
			  ["I",     "Инфекционные и паразитарные болезни"]
			, ["II",    "Новообразования"]
			, ["III",   "Болезни крови, кроветворных органов и отдельные нарушения, вовлекающие иммунный механизм"]
			, ["IV",    "Болезни эндокринной системы, расстройства питания и нарушения обмена веществ"]
			, ["V",     "Психические расстройства и расстройства поведения"]
			, ["VI",    "Болезни нервной системы"]
			, ["VII",   "Болезни глаза и его придаточного аппарата"]
			, ["VIII",  "Болезни уха и сосцевидного отростка"]
			, ["IX",    "Болезни системы кровообращения"]
			, ["X",     "Болезни органов дыхания"]
			, ["XI",    "Болезни органов пищеварения"]
			, ["XII",   "Болезни кожи и подкожной клетчатки"]
			, ["XIII",  "Болезни костно-мышечной системы и соединительной ткани"]
			, ["XIV",   "Болезни мочеполовой системы"]
			, ["XV",    "Беременность, роды и послеродовой период"]
			, ["XVI",   "Отдельные состояния, возникающие в перинатальном периоде"]
			, ["XVII",  "Врожденные аномалии, деформации и хромосомные нарушения"]
			, ["XVIII", "Симптомы, признаки и отклонения от нормы, выявленные при клинических и лабораторных исследованиях, не классифицированные в других рубриках"]
			, ["XIX",   "Травмы, отравления и некоторые другие последствия воздействия внешних причин"]
			, ["XX",    "Внешние причины заболеваемости и смертности"]
			, ["XXI",   "Факторы, влияющие на состояние здоровья населения и обращения в учреждения здравоохранения"]
			, ["",      "Коды для особых целей"]
		];

		var controller = c_nailed(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			if (!this.model)
			{
				var model = [];
				for (var i = 0; i<items_description.length; i++)
				{
					var idescr = items_description[i];
					model.push({ КлассМКБ10: '' == idescr[0] ? '' : 'Класс ' + idescr[0], Наименование: idescr[1] });
				}
				this.SetFormContent(model);
			}
			base_Render.call(this, sel);

			$(sel + ' tr[fc-type="array-item"]').each(function (i)
			{
				var td_name = $(this).children('td.name');
				td_name.attr('title', td_name.text());
			});
			var self = this;
			$(sel + ' input[nailed-by="cpw"]').change(function () { self.CalcTotal(); });
			this.CalcTotal();
		}

		controller.CalcTotal= function()
		{
			var sel = this.fastening.selector;
			var AddValue= function()
			{
				var v = $(this).val();
				if ('' != v)
				{
					var iv = parseInt(v);
					if (!isNaN(iv))
						t += iv;
				}
			}
			var t = 0;
			$(sel + ' input[model-selector="Впервые_установлено.ПППГ"]').each(AddValue);
			$(sel + ' input.total0').val(t);
			t = 0;
			$(sel + ' input[model-selector="Впервые_установлено.ППГ"]').each(AddValue);
			$(sel + ' input.total1').val(t);
			t = 0;
			$(sel + ' input[model-selector="Впервые_установлено.ПГ"]').each(AddValue);
			$(sel + ' input.total2').val(t);
		}

		return controller;
	}
});
