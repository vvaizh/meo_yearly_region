define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/results/occupational/e_yearly_region_results_occupational.html'
],
function (c_nailed, tpl)
{
	return function()
	{
		var controller = c_nailed(tpl);

		return controller;
	}
});
