define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/results/coverage/e_myr_coverage.html'
],
function (c_nailed, tpl)
{
	return function()
	{
		var controller = c_nailed(tpl);

		return controller;
	}
});
