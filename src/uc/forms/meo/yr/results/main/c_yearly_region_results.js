define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/results/main/e_yearly_region_results.html'

	, 'forms/meo/yr/results/chronic/c_myr_chronic'
	, 'forms/meo/yr/results/occupational/c_yearly_region_results_occupational'
	, 'forms/meo/yr/results/research/c_yearly_region_results_research'
	, 'forms/meo/yr/results/expertise/c_yearly_region_results_expertise'
	, 'forms/meo/yr/results/verification/c_yearly_region_results_verification'
	, 'forms/meo/yr/results/coverage/c_myr_coverage'
],
function (c_nailed, tpl
	, c_myr_chronic
	, c_yearly_region_results_occupational
	, c_yearly_region_results_research
	, c_yearly_region_results_expertise
	, c_yearly_region_results_verification
	, c_myr_coverage
	)
{
	return function()
	{
		var options = {
			field_spec:
				{
					Хронические: { controller: c_myr_chronic }
					, Профзаболевания: { controller: c_yearly_region_results_occupational }
					, Исследования: { controller: c_yearly_region_results_research }
					, Экспертизы: { controller: c_yearly_region_results_expertise }
					, Проверки: { controller: c_yearly_region_results_verification }
					, Охват: { controller: c_myr_coverage }
				}
		};

		var controller = c_nailed(tpl, options);

		return controller;
	}
});
