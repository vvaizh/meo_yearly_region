define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/results/expertise/e_yearly_region_results_expertise.html'
],
function (c_nailed, tpl)
{
	return function()
	{
		var controller = c_nailed(tpl);

		return controller;
	}
});
