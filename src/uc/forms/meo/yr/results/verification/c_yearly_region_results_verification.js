define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/results/verification/e_yearly_region_results_verification.html'
],
function (c_nailed, tpl)
{
	return function()
	{
		var controller = c_nailed(tpl);

		return controller;
	}
});
