define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/results/research/e_yearly_region_results_research.html'
],
function (c_nailed, tpl)
{
	return function()
	{
		var controller = c_nailed(tpl);

		return controller;
	}
});
