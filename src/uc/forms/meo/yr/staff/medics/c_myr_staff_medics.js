define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/staff/medics/e_myr_staff_medics.html'
],
function (c_nailed, tpl)
{
	return function()
	{
		var controller = c_nailed(tpl);

		return controller;
	}
});
