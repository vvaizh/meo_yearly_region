define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/staff/main/e_myr_staff.html'
	, 'forms/meo/yr/staff/center/c_myr_staff_center'
	, 'forms/meo/yr/organizational/units/c_myr_org_units'
	, 'forms/meo/yr/staff/completeness/c_myr_staff_completeness'
	, 'forms/meo/yr/staff/medics/c_myr_staff_medics'
	, 'forms/meo/yr/staff/partition/c_myr_staff_partition'
	, 'forms/meo/yr/staff/cabinet/c_myr_staff_cabinet'
],
function (c_nailed, tpl
	, c_yearly_region_staff_center
	, c_myr_org_units
	, c_yearly_region_staff_completeness
	, c_yearly_region_staff_medics
	, c_yearly_region_staff_partition
	, c_yearly_region_staff_cabinet
	)
{
	return function()
	{
		var options = {
			field_spec:
				{
					Центр: { controller: c_yearly_region_staff_center }
					, Отделение_и_кабинет: { controller: c_yearly_region_staff_partition }
					, В_подразделениях: { controller: c_myr_org_units }
					, Укомплектованность: { controller: c_yearly_region_staff_completeness }
					, Врачи: { controller: c_yearly_region_staff_medics }
					, Кабинет: { controller: c_yearly_region_staff_cabinet }
				}
		};

		var controller = c_nailed(tpl, options);

		return controller;
	}
});
