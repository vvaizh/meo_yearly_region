define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/staff/partition/e_myr_staff_partition.html'
],
function (c_nailed, tpl)
{
	return function()
	{
		var controller = c_nailed(tpl);

		return controller;
	}
});
