define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/meo/yr/staff/cabinet/e_myr_staff_cabinet.html'
],
function (c_nailed, tpl)
{
	return function()
	{
		var controller = c_nailed(tpl);

		return controller;
	}
});
