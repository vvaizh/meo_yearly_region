﻿define(function ()
{

	var regions =
		[
   ["Республика Коми"]
 , ["Волгоградская область"]
 , ["г. Москва"]
 , ["Мурманская область"]
 , ["Удмуртская Республика"]
 , ["Калининградская область"]
 , ["Пермский край"]
 , ["Республика Алтай"]
 , ["Свердловская область"]
 , ["Ханты-Мансийский автономный округ"]
 , ["Московская область"]
 , ["Республика Татарстан"]
 , ["Нижегородская область"]
 , ["г. Санкт-Петербург"]
 , ["Республика Калмыкия"]
 , ["Республика Дагестан"]
 , ["Республика Мордовия"]
 , ["Ульяновская область"]
 , ["Новгородская область"]
 , ["Ростовская область"]
 , ["Ставропольский край"]
 , ["Чувашская Республика"]
 , ["Рязанская область"]
 , ["Краснодарский край"]
 , ["Самарская область"]
 , ["Вологодская область"]
 , ["Тюменская область"]
 , ["Хабаровский край"]
 , ["Омская область"]
 , ["Астраханская область"]
 , ["Смоленская область"]
 , ["Республика Башкортостан"]
 , ["Ленинградская область"]
 , ["Кемеровская область"]
 , ["Республика Ингушетия"]
 , ["Саратовская область"]
 , ["Калужская область"]
 , ["Ярославская область"]
 , ["Курганская область"]
 , ["Еврейская авт. область"]
 , ["Курская область"]
 , ["Приморский край"]
 , ["Липецкая область"]
 , ["Алтайский край"]
 , ["Кировская область"]
 , ["Владимирская область"]
 , ["Тверская область"]
 , ["Красноярский край"]
 , ["Воронежская область"]
 , ["Тульская область"]
 , ["Сахалинская область"]
 , ["Иркутская область"]
 , ["Орловская область"]
 , ["Томская область"]
 , ["Ямало-Ненецкий автономный округ"]
 , ["Республика Карелия"]
 , ["Республика Хакасия"]
 , ["Забайкальский край"]
 , ["Костромская область"]
 , ["Республика Тыва (Тува)"]
 , ["Архангельская область"]
 , ["Пензенская область"]
 , ["Новосибирская область"]
 , ["Псковская область"]
 , ["Ивановская область"]
 , ["Северная Осетия"]
 , ["Тамбовская область"]
 , ["Челябинская область"]
 , ["Брянская область"]
 , ["Республика Саха (Якутия)"]
 , ["Амурская область"]
 , ["Республика Бурятия"]
 , ["Белгородская область"]
 , ["Оренбургская область"]
 , ["Магаданская область"]
 , ["Камчатский край"]
 , ["Республика Адыгея"]
 , ["Кабардино-Балкарская Республика"]
 , ["Республика Марий Эл"]
 , ["Карачаево-Черкесская Республика"]
 , ["Ненецкий автономный округ"]
 , ["Республика Крым"]
 , ["Чукотский автономный округ"]
 , ["г. Севастополь"]
 , ["г. Байконур"]
 , ["Чеченская Республика"]
 , ["Беловодье"]
		];

	var helper =
	{
		array: regions
		, FindForSelect2: function (term)
		{
			var res = [];
			var fixed_term = term.toUpperCase();
			for (var i = 0; i < regions.length; i++)
			{
				var region = regions[i];
				for (var j = 0; j < region.length; j++)
				{
					var region_name = region[j];
					var fixed_region_name = region_name.toUpperCase();
					if (-1 != fixed_region_name.indexOf(fixed_term))
					{
						res.push({ id: region[0], text: region[0] });
						break;
					}
				}
			}
			return res;
		}
	};
	return helper;
})
