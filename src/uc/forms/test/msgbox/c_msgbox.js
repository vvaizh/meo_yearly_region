﻿define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/test/msgbox/v_msgbox.html'
	, 'forms/base/h_msgbox'
	, 'tpl!forms/test/msgbox/v_pretty_msgbox_content.html'
	, 'forms/test/nailed/simple/c_simple'
],
function (c_nailed, tpl, h_msgbox, v_pretty_msgbox_content, c_simple)
{
	return function ()
	{
		var controller = c_nailed(tpl);

		var base_Render= controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this,sel);
			var self= this;
			$(sel + ' button.static-txt').click(function(e){e.preventDefault();self.OnStaticText();});
			$(sel + ' button.static-tpl').click(function(e){e.preventDefault();self.OnStaticTmpl();});
			$(sel + ' button.controller').click(function(e){e.preventDefault();self.OnController();});
			$(sel + ' button.ajax'      ).click(function(e){e.preventDefault();self.OnAjax();});
		}

		controller.OnStaticText= function()
		{
			h_msgbox.ShowModal({title:'форма со статическим текстом', html:'вручную набранный <b>html</b> text'});
		}

		controller.OnStaticTmpl= function()
		{
			h_msgbox.ShowModal
			({
				  title:'форма с результатом html шаблона 400*400'
				, html:v_pretty_msgbox_content({Текущее_время:new Date()})
				, width:400, height:400
			});
		}

		controller.OnController= function()
		{
			var btnOk= 'Сохранить';
			if (!this.simple_content)
				this.simple_content= {};
			var simple= c_simple();
			simple.SetFormContent(this.simple_content);
			var self = this;
			h_msgbox.ShowModal
			({
				  title:'форма с controller ом 400*250'
				, controller: simple
				, width:400, height:250
				, buttons:[btnOk, 'Отмена']
				, onclose: function(btn)
				{
					if (btn == btnOk)
						self.simple_content = simple.GetFormContent();
				}
			});
		}

		controller.OnAjax= function()
		{
			var ajaxurl= "http://левый.урл";

			h_msgbox.Show({
				width: 500
				, title: 'Передача данных на несуществующий адрес'
				, html: '<center>Передаём данные на адрес ' + ajaxurl + '</center>'
			});

			$.ajax({
				  url: ajaxurl
				, dataType: "json"
				, type: 'POST'
				, data: { a: "b" }
				, cache: false
				, error: function (data, textStatus)
				{
					h_msgbox.Close();
					h_msgbox.ShowAjaxError('Отправка данных на несуществующий адрес', ajaxurl, data, textStatus);
				}
				, success: function (data, textStatus)
				{
					h_msgbox.Close();
					h_msgbox.ShowAjaxError('Получение данных с несуществующего адреса', ajaxurl, data, textStatus);
				}
			});
		}

		return controller;
	}
});