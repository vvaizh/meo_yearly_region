define
(
	[
		  'forms/base/collector'
		, 'forms/test/core/xml/controller'
		, 'forms/test/core/tpl/controller'
		, 'forms/test/core/min/controller'
		, 'forms/test/core/base/controller'
		, 'forms/test/styles/c_styles'
		, 'forms/test/nailed/fields/c_nailed_fields'
		, 'forms/test/collection/simple/c_test_collection'
		, 'forms/test/nailed/simple/c_simple'
		, 'forms/test/nailed/containers/c_containers'
		, 'forms/test/msgbox/c_msgbox'
		, 'forms/test/nailed/a_modal/c_a_modal'
		, 'forms/test/collection/subset/c_subset'
		, 'forms/test/collection/editable/c_collection_editable'
		, 'forms/test/collection/grid/local/c_test_grid'
		, 'forms/test/collection/grid/ajax/c_test_grid_ajax'
		, 'forms/test/collection/inline/c_collection_inline'
		, 'forms/test/nailed/select2/c_select2'
		, 'forms/test/nailed/typeahead/c_typeahead'
		, 'forms/test/complex/view/c_test_complex_view'
		, 'forms/test/nailed/with/c_with'
		, 'forms/test/nailed/with_collection/c_with_collection'
		, 'forms/test/collection/simplest/c_test_collection0'
	],
	function (collect)
	{
		return collect([
		  'xml'
		, 'tpl'
		, 'min'
		, 'base'
		, 'styles'
		, 'fields'
		, 'collection'
		, 'simple'
		, 'containers'
		, 'msgbox'
		, 'a_modal'
		, 'subset'
		, 'editable_collection'
		, 'grid'
		, 'grid_ajax'
		, 'collection_inline'
		, 'select2'
		, 'typeahead'
		, 'complex_view'
		, 'with'
		, 'with_collection'
		, 'simplest_collection'
		], Array.prototype.slice.call(arguments,1));
	}
);