﻿define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/test/nailed/with_collection/e_with_collection.html'
],
function (c_nailed, tpl)
{
	return function ()
	{
		var controller = c_nailed(tpl);

		return controller;
	}
});