﻿define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/test/nailed/a_modal/e_a_modal.html'
	, 'forms/test/nailed/simple/c_simple'
	, 'forms/base/h_msgbox'
],
function (c_nailed, tpl, c_simple, h_msgbox)
{
	return function ()
	{
		var options= {
			field_spec:
			{
				Подшефный:
				{
					  controller: c_simple
					, title: 'Редактирование подшефного'
					, width: 400, height:250
					, text: function(п)
					{
						return !п || null==п 
						? 'Кликните, чтобы указать данные..'
						: п.Ученик + '(алгебра:' + п.Оценки.алгебра + ', пение:' + п.Оценки.пение + ')';
					}
				}
			}
		};
		var controller = c_nailed(tpl, options);
		return controller;
	}
});