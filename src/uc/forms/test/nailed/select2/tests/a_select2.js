﻿define
([
	'forms/test/nailed/select2/h_select2_variants'
	, 'forms/base/ajax/ajax-select2'
	, 'forms/base/codec/codec.copy'
]
, function (h_select2_variants, ajax_select2, codec_copy)
{
	var transport = ajax_select2();

	transport.query= function(q,page)
	{
		var maxSize = 6;
		var items = [];
		var i = 0;
		for (; i < h_select2_variants.length && items.length<maxSize; i++)
		{
			var item= h_select2_variants[i];
			if (-1 != item.text.indexOf(q))
				items.push(h_select2_variants[i]);
		}
		var more = items.length == maxSize && i < h_select2_variants.length;
		return { results: items, pagination: { more: more } };
	}

	transport.options.url_prefix = 'test/select2/sixth';

	return transport.prepare_try_to_prepare_send_abort();
});