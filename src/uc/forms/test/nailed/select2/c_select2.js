﻿define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/test/nailed/select2/e_select2.html'
	, 'forms/test/nailed/select2/h_select2_variants'
],
function (c_nailed, tpl, h_select2_variants)
{
	return function ()
	{
		var select_variants = h_select2_variants;

		var options = {
			field_spec:
				{
					second: { data: select_variants }
					, third:
						{
							query: function (cb)
							{
								var data = { results: [] };
								for (var i = 0; i < select_variants.length && data.results.length<2; i++)
								{
									var variant = select_variants[i];
									if (-1 != variant.text.indexOf(cb.term))
										data.results.push(variant);
								}
								cb.callback(data);
							}
						}
					, fiveth: { data: select_variants, multiple: true }
					, sixth: { ajax: { url: 'test/select2/sixth', dataType: 'json' } }
					, seventh: { multiple: true, ajax: { url: 'test/select2/sixth', dataType: 'json' } }
				}
		};

		var controller = c_nailed(tpl, options);

		return controller;
	}
});