﻿define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/test/collection/editable/e_collection_editable.html'
	, 'txt!forms/test/collection/simple/tests/contents/test3.json.txt'
	, 'forms/base/codec/codec.copy'
	, 'forms/base/h_msgbox'
	, 'forms/test/nailed/simple/c_simple'
],
function (c_nailed, tpl, set, copy_codec, h_msgbox, c_simple)
{
	return function ()
	{
		var options=
			{
				  ccopy: copy_codec()
				, h_msgbox: h_msgbox
				, c_simple: c_simple
			};
		var controller = c_nailed(tpl, options);
		return controller;
	}
});