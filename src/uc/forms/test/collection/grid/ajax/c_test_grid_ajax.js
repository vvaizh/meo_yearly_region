﻿define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/test/collection/grid/local/e_test_grid.html'
	, 'forms/base/h_msgbox'
	, 'forms/test/nailed/simple/c_simple'
	, 'tpl!forms/test/collection/grid/local/v_test_grid_remove_confirm.html'
],
function (c_nailed, tpl, h_msgbox, c_simple, tpl_remove_confirm)
{
	return function ()
	{
		var controller = c_nailed(tpl);

		var base_Render= controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this,sel);
			this.RenderGrid();

			var self= this;
			$(sel + ' a.button.add').button().click(function(e){e.preventDefault();self.OnAdd();});
			$(sel + ' a.button.edit').button().click(function(e){e.preventDefault();self.OnEdit();});
			$(sel + ' a.button.remove	').button().click(function(e){e.preventDefault();self.OnDelete();});
		}

		controller.colModel =
		[
			  { name: 'id', hidden: true }
			, { label: 'Ученик', name: 'Ученик' }
			, { label: 'Алгебра', name: 'алгебра', jsonmap: 'Оценки.алгебра' }
			, { label: 'Пение', name: 'пение', jsonmap: 'Оценки.пение' }
		];

		controller.base_grid_url = 'test/grid/rows';
		controller.PrepareUrl = function ()
		{
			return this.base_grid_url;
		}

		controller.RenderGrid= function()
		{
			var sel = this.fastening.selector;
			var self= this;

			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'json'
				, url: self.PrepareUrl()
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка...'
				, recordtext: 'Ученики {0} - {1} из {2}'
				, emptyText: 'Нет учеников для просмотра'
				, rownumbers: false
				, rowNum: 10
				, rowList: [5, 10, 15]
				, pager: '#crowbar-test-grid-pager'
				, viewrecords: true
				, height: 'auto'
				, width: '800'
				, multiselect: true
				, multiboxonly: true
				, ignoreCase: true
				, ondblClickRow: function () { self.OnEdit(); }
				, onSelectRow: function () { self.OnSelect(); }
				, onSelectAll: function () { self.OnSelect(); }
				, jsonReader: { repeatitems: false, root: "rows", userdata: "rows" }
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnSelect = function ()
		{
			this.UpdateButtonsState();
		}

		controller.UpdateButtonsState = function ()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selected = grid.jqGrid('getGridParam', 'selarrrow').length;
			$(sel + ' a.button.edit').css('display', (1 == selected) ? 'inline-block' : 'none');
			$(sel + ' a.button.remove').css('display', (0 != selected) ? 'inline-block' : 'none');
		}

		var FindByField= function(rows,name,value)
		{
			if (rows)
			{
				for (var i = 0; i < rows.length; i++)
				{
					var row = rows[i];
					if (value == row[name])
						return row;
				}
			}
			return null;
		}

		controller.OnEdit= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var self= this;
			var selected = grid.jqGrid('getGridParam', 'selarrrow');
			if (1 == selected.length)
			{
				var rows = grid.jqGrid('getGridParam', 'userData');
				var edited_grid_row = FindByField(rows, 'id', selected[0]);
				var simple= c_simple();
				simple.SetFormContent(edited_grid_row);
				var btnOk= 'Сохранить';
				h_msgbox.ShowModal
				({
					  title:'Редактирование записи'
					, controller: simple
					, width:400, height:250
					, buttons:[btnOk, 'Отмена']
					, onclose: function(btn)
					{
						if (btn==btnOk)
						{
							if (!self.fastening.model)
								self.fastening.model = [];
							self.fastening.model[edited_grid_row.id] = simple.GetFormContent();
							self.ReloadGrid();
							$(sel).trigger('model_change');
						}
					}
				});
			}
		}

		controller.OnAdd= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var self= this;
			var simple= c_simple();
			simple.SetFormContent({});
			var btnOk= 'Сохранить';
			h_msgbox.ShowModal
			({
				  title:'Добавление записи'
				, controller: simple
				, width:400, height:250
				, buttons:[btnOk, 'Отмена']
				, onclose: function(btn)
				{
					if (btn==btnOk)
					{
						if (!self.fastening.model)
							self.fastening.model = [];
						self.fastening.model.push(simple.GetFormContent());
						self.ReloadGrid();
						$(sel).trigger('model_change');
					}
				}
			});
		}

		controller.ReloadGrid= function()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			grid.jqGrid('GridUnload');
			this.RenderGrid();
			this.UpdateButtonsState();	
		}

		controller.OnDelete= function()
		{
			var self = this;
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selected = grid.jqGrid('getGridParam', 'selarrrow');
			if (0 != selected.length)
			{
				var to_delete = [];
				for (var i = 0; i < selected.length; i++)
					to_delete.push(grid.jqGrid('getRowData', selected[i]));
				h_msgbox.ShowModal
				({
					  html: tpl_remove_confirm(to_delete)
					, title: 'Подтверждение удаления'
					, width: '500'
					, buttons: ['Да', 'Нет']
					, onclose: function (bname)
					{
						if ('Да' == bname)
						{
							selected.sort();
							for (var i= selected.length-1; i>=0; i--)
								self.fastening.model.splice(selected[i], 1);
							self.ReloadGrid();
							$(sel).trigger('model_change');
						}
					}
				});
			}
		}

		var base_GetFormContent= controller.GetFormContent;
		controller.GetFormContent= function()
		{
			if (!this.fastening.model || null == this.fastening.model)
				this.fastening.model = [];
			var res = this.fastening.model;
			return res;
		}

		return controller;
	}
});