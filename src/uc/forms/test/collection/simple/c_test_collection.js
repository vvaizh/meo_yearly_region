﻿define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/test/collection/simple/e_test_collection.html'
],
function (c_nailed, tpl)
{
	return function ()
	{
		var controller = c_nailed(tpl);
		return controller;
	}
});