include ..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "из списка"

shot_check_png ..\..\shots\01sav.png

je $('div.cpw-collection [array-index="1"] .do_delete').click();

shot_check_png ..\..\shots\02edt.png

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\..\..\simple\tests\contents\01edt-1.json.result.txt
exit