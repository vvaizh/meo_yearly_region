﻿define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/test/collection/subset/e_subset.html'
	, 'txt!forms/test/collection/simple/tests/contents/test3.json.txt'
	, 'forms/base/codec/codec.copy'
	, 'forms/base/nailing/fastening/h_fastening_clip'
],
function (c_nailed, tpl, set, copy_codec, h_fastening_clip)
{
	return function ()
	{
		var options=
			{
				  foreign_data: JSON.parse(set)
				, ccopy: copy_codec()
			};
		var controller = c_nailed(tpl, options);


		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' table.collection tbody').sortable({
				items: "tr[fc-type='array-item']"
				, handle: '.handle'
				, stop: function (e, ui) { self.OnItemMove(e, ui); }
			});
			$(sel + ' a.up').click(function (e) { e.preventDefault(); self.OnItemUp(e); });
			$(sel + ' a.down').click(function (e) { e.preventDefault(); self.OnItemDown(e); });
		}

		controller.OnItemMove= function(e, ui)
		{
			var indexes = []
			$(e.target).children().each(function (index)
			{
				var index = $(this).attr('array-index');
				if (index)
					indexes.push(parseInt(index));
			});
			$(e.target).sortable('cancel');
			var fc_dom_item = $(this.selector);
			var model = this.fastening.get_fc_model_value(fc_dom_item);
			var tmp_model = [];
			for (var i = 0; i < model.length; i++)
				tmp_model[i] = model[i];
			for (var i = 0; i < model.length; i++)
				model[i] = tmp_model[indexes[i]];
			this.fastening.set_fc_model_value(fc_dom_item, model);
			fc_dom_item.trigger('model_change');
		}

		controller.OnItemUp= function(e)
		{
			var i_item = h_fastening_clip.get_parent_fc_of_type($(e.target), 'array-item').attr('array-index');
			i_item = parseInt(i_item);
			if (0 < i_item)
			{
				var fc_dom_item = $(this.selector);
				var model = this.fastening.get_fc_model_value(fc_dom_item);

				var tmp = model[i_item - 1];
				model[i_item - 1] = model[i_item];
				model[i_item] = tmp;

				this.fastening.set_fc_model_value(fc_dom_item, model);
				fc_dom_item.trigger('model_change');
			}
		}

		controller.OnItemDown = function (e)
		{
			var i_item = h_fastening_clip.get_parent_fc_of_type($(e.target), 'array-item').attr('array-index');
			i_item = parseInt(i_item);
			var fc_dom_item = $(this.selector);
			var model = this.fastening.get_fc_model_value(fc_dom_item);
			if (i_item < (model.length-1))
			{
				var tmp = model[i_item + 1];
				model[i_item + 1] = model[i_item];
				model[i_item] = tmp;

				this.fastening.set_fc_model_value(fc_dom_item, model);
				fc_dom_item.trigger('model_change');
			}
		}

		return controller;
	}
});