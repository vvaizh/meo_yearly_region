define([
	  'forms/test/core/tpl/controller'
	, 'forms/test/core/xml/codec.xml'
],
function (BaseTplController, codec_xml)
{
	return function ()
	{
		var res = BaseTplController();
		res.UseCodec(codec_xml());
		return res;
	}
});
