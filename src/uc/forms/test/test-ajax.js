define
(
	[
		  'forms/base/ajax/ajax-collector'
		, 'forms/test/collection/grid/ajax/tests/a_test_grid_ajax'
		, 'forms/test/nailed/select2/tests/a_select2'
	],
	function (ajax_collector)
	{
		return ajax_collector(Array.prototype.slice.call(arguments, 1));
	}
);