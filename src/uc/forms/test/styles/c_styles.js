﻿define([
	  'forms/base/nailing/c_nailed'
	, 'tpl!forms/test/styles/v_styles.html'
],
function (c_nailed, tpl)
{
	return function ()
	{
		var controller = c_nailed(tpl);
		return controller;
	}
});