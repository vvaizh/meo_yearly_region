require.config
({
  enforceDefine: true,
  urlArgs: "bust" + (new Date()).getTime(),
  baseUrl: '.',
  paths:
  {
    styles: 'css',
    images: 'images'
  },
  map:
  {
    '*':
    {
      tpl:   'js/libs/tpl',
      css:   'js/libs/css',
      image: 'js/libs/image',
      txt:   'js/libs/txt'
    }
  }
});

require([
	  'forms/base/nailing/test/h_nailed_field_testers'
], function (h_nailed_field_testers)
{
	wbt = {};

	wbt.SetModelFieldValue= function(model_selector, value)
	{
		var sel = this.Model_selector_prefix + '[model-selector="' + model_selector + '"]';
		return wbt.ActionBySpec(sel, function (dom_item) { return wbt.SetValue(dom_item, value, sel); });
	}

	wbt.CheckModelFieldValue = function (model_selector, value)
	{
		var sel = this.Model_selector_prefix + '[model-selector="' + model_selector + '"]';
		return wbt.ActionBySpec(sel, function (dom_item) { return wbt.CheckValue(dom_item, value); });
	}

	wbt.Model_selector_prefix = '';

	wbt.Push_workspace= function(selector)
	{
		this.prev_context =
			{
				prev_selector: this.prev_selector
				, Model_selector_prefix: this.Model_selector_prefix
			};
		this.Model_selector_prefix += selector + ' ';
		return (window.wbt_CheckMode) ? null : 'current selector prefix is \"' + this.Model_selector_prefix + '\"';
	}

	wbt.Pop_workspace= function()
	{
		this.Model_selector_prefix = !this.prev_context || null == this.prev_context ? '' : this.prev_context.Model_selector_prefix;
		this.prev_context = !this.prev_context || null == this.prev_context ? null : this.prev_context.prev_context;
		return (window.wbt_CheckMode) ? null : 'current selector prefix is \"' + this.Model_selector_prefix + '\"';
	}

	wbt.Push_workspace_model_selector= function(model_selector)
	{
		return this.Push_workspace('[model-selector="' + model_selector + '"]');
	}

	wbt.ActionBySpec = function (sel, action)
	{
		try
		{
			var dom_item = $(sel);
			if (!dom_item || null == dom_item || 0 == dom_item.length)
			{
				return 'can not find element for selector "' + sel + '"';
			}
			else
			{
				var res = action(dom_item);
				return null == res ? null :
					'wait_long_process!' == res ? res : '\'' + sel + '\': ' + res;
			}
		}
		catch (ex)
		{
			return 'exception ' + ex.toString();
		}
	}

	wbt.SetValue = function (dom_item, value, model_selector)
	{
		var field_tester = h_nailed_field_testers.find_tester_for(dom_item);
		if (!window.wbt_CheckMode)
		{
			return field_tester.set_value(dom_item, value, model_selector);
		}
		else
		{
			return field_tester.check_value(dom_item, value, /* instead_of_set= */true);
		}
	}

	wbt.CheckValue = function (dom_item, value)
	{
		var field_tester = h_nailed_field_testers.find_tester_for(dom_item);
		return field_tester.check_value(dom_item, value);
	}

});
