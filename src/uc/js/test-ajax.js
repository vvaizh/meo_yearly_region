require.config
({
	enforceDefine: true,
	urlArgs: "bust" + (new Date()).getTime(),
	baseUrl: '.',
	map:
	{
		'*':
		{
			txt: 'js/libs/txt'
		}
	}
});

require([
	  'forms/base/ajax/ajax-collector'
	, 'forms/test/test-ajax'
], function (ajax_collector)
{
	var transports= Array.prototype.slice.call(arguments, 1);

	$.ajaxTransport
	(
		'+*',
		function (options, originalOptions, jqXHR)
		{
			return ajax_collector(transports)(options, originalOptions, jqXHR);
		}
	);

	if (OnLoadTestTransports)
		OnLoadTestTransports();
});
