cpw_ArgumentsProcessor =
{
	getJsonFromUrl: function ()
	{
		var txt_arguments = location.search.substr(1);
		var arguments = {};
		var txt_argument_parts = txt_arguments.split('&');
		for (var i = 0; i < txt_argument_parts.length; i++)
		{
			var txt_argument_part = txt_argument_parts[i];
			if (0 != txt_argument_part)
			{
				var txt_argument_part_parts = txt_argument_part.split('=');
				if (1 >= txt_argument_part_parts.length)
				{
					arguments[txt_argument_part] = true;
				}
				else
				{
					arguments[txt_argument_part_parts[0]] = decodeURIComponent(txt_argument_part_parts[1]);
				}
			}
		}
		return arguments;
	}

	, CreateNew: function (id_form)
	{
		if (id_form.indexOf('#') != -1)
			id_form = id_form.substring(0, id_form.length - 1);
		$("#cpw-form-select [value='" + id_form + "']").attr("selected", "selected");
		var form_spec = GetSelectedFormSpecificationFor_id_Form(id_form);
		if (!form_spec)
		{
			alert('can not find specification for form \"' + id_Form + '\"');
		}
		else
		{
			CreateNewFormWithSpecFunc(form_spec);
		}
	}

	, ProcessContentArgument: function (id_form_content, do_for_form_spec)
	{
		var arg_parts = id_form_content.split('.');
		var id_content = arg_parts[0] + '.' + arg_parts[2];
		var id_form = arg_parts[0] + '.' + arg_parts[1];

		var content = GetSelectedSpecificationFor_id(CPW_contents, id_content);
		if (!content)
		{
			alert('can not find content for id \"' + id_content + '\"');
		}
		else
		{
			var content_area = $('#form-content-text-area');
			$('#form-content-text-area').val(content);
		}

		$("#cpw-form-select [value='" + id_form + "']").attr("selected", "selected");
		var form_spec_func = GetSelectedFormSpecificationFor_id_Form(id_form);
		if (!form_spec_func)
		{
			alert('can not find specification for form \"' + id_Form + '\"');
		}
		else
		{
			do_for_form_spec(form_spec_func);
		}
	}

	, ProcessArguments: function ()
	{
		var args = this.getJsonFromUrl();

		if (args.clear_profile)
		{
			app.profile = {};
		}

		if (args.use_test_timer)
		{
			app.cpw_Now = function () { return new Date(2015, 6, 26, 17, 36, 0, 0); };
		}

		if (args.view)
		{
			this.ProcessContentArgument(args.edit, EditFormWithSpecFunc);
		}
		else if (args.edit)
		{
			this.ProcessContentArgument(args.edit, EditFormWithSpecFunc);
		}
		else if (args.createnew)
		{
			this.CreateNew(args.createnew);
		}
	}
};
